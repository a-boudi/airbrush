# Airbrush

Angular adapter for [Graphiti](https://graphiti-api.github.io/graphiti) backends.

## Why?

NgxAirbrush is a library that aims to offer a spraypaint.js like library for Angular. Spraypaint.js uses typescript's library for HTTP queries while ngx-Airbrush uses angular's [HttpClient](https://angular.io/guide/http). This enables us to take fully advantage of HttpClient (interceptors, specs helpers, ...etc.).

## Documentation

Please see the [documentation page](https://gitlab.com/a-boudi/airbrush/tree/master/projects/ngx-airbrush)

## Thanks

This library is heavily inspired by [Angular2-jsonapi](https://github.com/ghidoz/angular2-jsonapi) and [spraypaint.js](https://github.com/graphiti-api/spraypaint.js).

## License

MIT © Abderrahmane Boudi
