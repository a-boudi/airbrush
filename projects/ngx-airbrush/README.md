# NgxAirbrush

A Jsonapi client for [Graphiti](https://graphiti-api.github.io/graphiti) powered backends.

## Installation

```bash
$ npm install ngx-airbrush
```

## Usage

Each resource is a combination of a model and its service. The model is used to the attributes and relationships. The service is used to fetch and persist data from/to the backend.
