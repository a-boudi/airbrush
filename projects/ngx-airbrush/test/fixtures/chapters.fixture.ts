import { Chapter } from '../models/models.index';
import { ChapterType } from '../models/types.index';
import { JsonapiDocument } from '../../src/lib/interfaces/jsonapi.interface';


export const CHAPTER_1_ID      = '1';
export const CHAPTER_1_TITLE   = 'Chapter 1';
export const CHAPTER_1_ORDER   = 1;
export const CHAPTER_1_CREATED = '2020-03-26T17:19:54.000Z';
export const CHAPTER_1_UPDATED = '2020-03-30T17:19:54.000Z';

export const chapter1: Partial<Chapter> = {
  id       : CHAPTER_1_ID,
  title    : CHAPTER_1_TITLE,
  ordering : CHAPTER_1_ORDER,
  createdAt: new Date(CHAPTER_1_CREATED),
  updatedAt: new Date(CHAPTER_1_UPDATED),
};

export const chapter1js: JsonapiDocument = {
  data: {
    id: CHAPTER_1_ID,
    type: ChapterType,
    attributes: {
      title    : CHAPTER_1_TITLE,
      ordering : CHAPTER_1_ORDER,
      createdAt: CHAPTER_1_CREATED,
      updatedAt: CHAPTER_1_UPDATED,
    },
  },
};

export const CHAPTER_2_ID      = '2';
export const CHAPTER_2_TITLE   = 'Chapter 2';
export const CHAPTER_2_ORDER   = 2;
export const CHAPTER_2_CREATED = '2020-03-26T17:21:54.000Z';
export const CHAPTER_2_UPDATED = '2020-03-30T17:21:54.000Z';

export const chapter2: Partial<Chapter> = {
  id       : CHAPTER_2_ID,
  title    : CHAPTER_2_TITLE,
  ordering : CHAPTER_2_ORDER,
  createdAt: new Date(CHAPTER_2_CREATED),
  updatedAt: new Date(CHAPTER_2_UPDATED),
};

export const chapter2js: JsonapiDocument = {
  data: {
    id: CHAPTER_2_ID,
    type: ChapterType,
    attributes: {
      title    : CHAPTER_2_TITLE,
      ordering : CHAPTER_2_ORDER,
      createdAt: CHAPTER_2_CREATED,
      updatedAt: CHAPTER_2_UPDATED,
    },
  },
};

export const CHAPTER_3_ID      = '3';
export const CHAPTER_3_TITLE   = 'Chapter 19';
export const CHAPTER_3_ORDER   = 19;
export const CHAPTER_3_CREATED = '2020-03-26T17:25:54.000Z';
export const CHAPTER_3_UPDATED = '2020-03-30T17:25:54.000Z';

export const chapter3: Partial<Chapter> = {
  id       : CHAPTER_3_ID,
  title    : CHAPTER_3_TITLE,
  ordering : CHAPTER_3_ORDER,
  createdAt: new Date(CHAPTER_3_CREATED),
  updatedAt: new Date(CHAPTER_3_UPDATED),
};

export const chapter3js: JsonapiDocument = {
  data: {
    id: CHAPTER_3_ID,
    type: ChapterType,
    attributes: {
      title    : CHAPTER_3_TITLE,
      ordering : CHAPTER_3_ORDER,
      createdAt: CHAPTER_3_CREATED,
      updatedAt: CHAPTER_3_UPDATED,
    },
  },
};

export const CHAPTER_4_ID      = '4';
export const CHAPTER_4_TITLE   = 'Chapter 4';
export const CHAPTER_4_ORDER   = 4;
export const CHAPTER_4_CREATED = '2020-04-02T10:50:12.000Z';
export const CHAPTER_4_UPDATED = '2020-04-02T10:50:12.000Z';

export const chapter4: Partial<Chapter> = {
  id       : CHAPTER_4_ID,
  title    : CHAPTER_4_TITLE,
  ordering : CHAPTER_4_ORDER,
  createdAt: new Date(CHAPTER_4_CREATED),
  updatedAt: new Date(CHAPTER_4_UPDATED),
};

export const chapter4js: JsonapiDocument = {
  data: {
    id: CHAPTER_4_ID,
    type: ChapterType,
    attributes: {
      title    : CHAPTER_4_TITLE,
      ordering : CHAPTER_4_ORDER,
      createdAt: CHAPTER_4_CREATED,
      updatedAt: CHAPTER_4_UPDATED,
    },
  },
};

export const CHAPTER_5_ID      = '5';
export const CHAPTER_5_TITLE   = 'Chapter 15';
export const CHAPTER_5_ORDER   = 15;
export const CHAPTER_5_CREATED = '2020-04-02T10:55:37.000Z';
export const CHAPTER_5_UPDATED = '2020-04-02T10:55:37.000Z';

export const chapter5: Partial<Chapter> = {
  id       : CHAPTER_5_ID,
  title    : CHAPTER_5_TITLE,
  ordering : CHAPTER_5_ORDER,
  createdAt: new Date(CHAPTER_5_CREATED),
  updatedAt: new Date(CHAPTER_5_UPDATED),
};

export const chapter5js: JsonapiDocument = {
  data: {
    id: CHAPTER_5_ID,
    type: ChapterType,
    attributes: {
      title    : CHAPTER_5_TITLE,
      ordering : CHAPTER_5_ORDER,
      createdAt: CHAPTER_5_CREATED,
      updatedAt: CHAPTER_5_UPDATED,
    },
  },
};
