import { Book } from '../models/models.index';
import { BookType } from '../models/types.index';
import { JsonapiDocument } from '../../src/lib/interfaces/jsonapi.interface';


export const BOOK_1_ID        = '1';
export const BOOK_1_TITLE     = 'The Fellowship of the Ring';
export const BOOK_1_PUBLISHED = '1954-07-29';
export const BOOK_1_CREATED   = '2020-03-26T17:19:54.000Z';
export const BOOK_1_UPDATED   = '2020-03-30T17:19:54.000Z';

export const book1: Partial<Book> = {
  id           : BOOK_1_ID,
  title        : BOOK_1_TITLE,
  datePublished: new Date(BOOK_1_PUBLISHED),
  createdAt    : new Date(BOOK_1_CREATED),
  updatedAt    : new Date(BOOK_1_UPDATED),
};

export const book1js: JsonapiDocument = {
  data: {
    id: BOOK_1_ID,
    type: BookType,
    attributes: {
      title        : BOOK_1_TITLE,
      datePublished: BOOK_1_PUBLISHED + 'T00:00:00.000Z',
      createdAt    : BOOK_1_CREATED,
      updatedAt    : BOOK_1_UPDATED,
    },
  },
};

export const BOOK_2_ID        = '2';
export const BOOK_2_TITLE     = 'The Two Towers';
export const BOOK_2_PUBLISHED = '1954-11-11';
export const BOOK_2_CREATED   = '2020-03-26T17:21:54.000Z';
export const BOOK_2_UPDATED   = '2020-03-30T17:21:54.000Z';

export const book2: Partial<Book> = {
  id           : BOOK_2_ID,
  title        : BOOK_2_TITLE,
  datePublished: new Date(BOOK_2_PUBLISHED),
  createdAt    : new Date(BOOK_2_CREATED),
  updatedAt    : new Date(BOOK_2_UPDATED),
};

export const book2js: JsonapiDocument = {
  data: {
    id: BOOK_2_ID,
    type: BookType,
    attributes: {
      title        : BOOK_2_TITLE,
      datePublished: BOOK_2_PUBLISHED + 'T00:00:00.000Z',
      createdAt    : BOOK_2_CREATED,
      updatedAt    : BOOK_2_UPDATED,
    },
  },
};

export const BOOK_3_ID        = '3';
export const BOOK_3_TITLE     = 'The Return of the King';
export const BOOK_3_PUBLISHED = '1955-10-20';
export const BOOK_3_CREATED   = '2020-03-26T17:25:54.000Z';
export const BOOK_3_UPDATED   = '2020-03-30T17:25:54.000Z';

export const book3: Partial<Book> = {
  id           : BOOK_3_ID,
  title        : BOOK_3_TITLE,
  datePublished: new Date(BOOK_3_PUBLISHED),
  createdAt    : new Date(BOOK_3_CREATED),
  updatedAt    : new Date(BOOK_3_UPDATED),
};

export const book3js: JsonapiDocument = {
  data: {
    id: BOOK_3_ID,
    type: BookType,
    attributes: {
      title        : BOOK_3_TITLE,
      datePublished: BOOK_3_PUBLISHED + 'T00:00:00.000Z',
      createdAt    : BOOK_3_CREATED,
      updatedAt    : BOOK_3_UPDATED,
    },
  },
};

export const BOOK_4_ID        = '4';
export const BOOK_4_TITLE     = 'The Nameless City';
export const BOOK_4_PUBLISHED = '1921-11-1';
export const BOOK_4_CREATED   = '2020-04-02T10:50:12.000Z';
export const BOOK_4_UPDATED   = '2020-04-02T10:50:12.000Z';

export const book4: Partial<Book> = {
  id           : BOOK_4_ID,
  title        : BOOK_4_TITLE,
  datePublished: new Date(BOOK_4_PUBLISHED),
  createdAt    : new Date(BOOK_4_CREATED),
  updatedAt    : new Date(BOOK_4_UPDATED),
};

export const book4js: JsonapiDocument = {
  data: {
    id: BOOK_4_ID,
    type: BookType,
    attributes: {
      title        : BOOK_4_TITLE,
      datePublished: BOOK_4_PUBLISHED + 'T00:00:00.000Z',
      createdAt    : BOOK_4_CREATED,
      updatedAt    : BOOK_4_UPDATED,
    },
  },
};

export const BOOK_5_ID        = '5';
export const BOOK_5_TITLE     = 'The Shadow over Innsmouth';
export const BOOK_5_PUBLISHED = '1936-04-1';
export const BOOK_5_CREATED   = '2020-04-02T10:55:37.000Z';
export const BOOK_5_UPDATED   = '2020-04-02T10:55:37.000Z';

export const book5: Partial<Book> = {
  id           : BOOK_5_ID,
  title        : BOOK_5_TITLE,
  datePublished: new Date(BOOK_5_PUBLISHED),
  createdAt    : new Date(BOOK_5_CREATED),
  updatedAt    : new Date(BOOK_5_UPDATED),
};

export const book5js: JsonapiDocument = {
  data: {
    id: BOOK_5_ID,
    type: BookType,
    attributes: {
      title        : BOOK_5_TITLE,
      datePublished: BOOK_5_PUBLISHED + 'T00:00:00.000Z',
      createdAt    : BOOK_5_CREATED,
      updatedAt    : BOOK_5_UPDATED,
    },
  },
};
