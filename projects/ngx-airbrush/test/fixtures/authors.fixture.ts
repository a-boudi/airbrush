import { Author } from '../models/models.index';
import { AuthorType } from '../models/types.index';
import { JsonapiDocument } from '../../src/lib/interfaces/jsonapi.interface';
import * as bookFix from './books.fixture';


export const AUTHOR_1_ID      = '1';
export const AUTHOR_1_NAME    = 'J. R. R. Tolkien';
export const AUTHOR_1_BIRTH   = '1892-01-03';
export const AUTHOR_1_DEATH   = '1973-09-02';
export const AUTHOR_1_CREATED = '2020-03-26T17:12:54.000Z';
export const AUTHOR_1_UPDATED = '2020-03-30T10:01:37.000Z';
export const AUTHOR_1_WORDSCOUNT = 4676254;

export const author1: Partial<Author> = {
  id         : AUTHOR_1_ID,
  name       : AUTHOR_1_NAME,
  dateOfBirth: new Date(AUTHOR_1_BIRTH),
  createdAt  : new Date(AUTHOR_1_CREATED),
  updatedAt  : new Date(AUTHOR_1_UPDATED),
  wordsCount : AUTHOR_1_WORDSCOUNT,
};

export const author1js: JsonapiDocument = {
  data: {
    id: AUTHOR_1_ID,
    type: AuthorType,
    attributes: {
      name       : AUTHOR_1_NAME,
      dob        : AUTHOR_1_BIRTH + 'T00:00:00.000Z',
      createdAt  : AUTHOR_1_CREATED,
      updatedAt  : AUTHOR_1_UPDATED,
      wordsCount : AUTHOR_1_WORDSCOUNT,
    },
  },
};


export const AUTHOR_2_ID      = '2';
export const AUTHOR_2_NAME    = 'H. P. Lovecraft';
export const AUTHOR_2_BIRTH   = '1890-08-20';
export const AUTHOR_2_DEATH   = '1937-03-15';
export const AUTHOR_2_CREATED = '2020-04-02T15:31:40.000Z';
export const AUTHOR_2_UPDATED = '2020-04-02T18:03:01.000Z';
export const AUTHOR_2_WORDSCOUNT = 3776254;

export const author2: Partial<Author> = {
  id         : AUTHOR_2_ID,
  name       : AUTHOR_2_NAME,
  dateOfBirth: new Date(AUTHOR_2_BIRTH),
  createdAt  : new Date(AUTHOR_2_CREATED),
  updatedAt  : new Date(AUTHOR_2_UPDATED),
  wordsCount : AUTHOR_2_WORDSCOUNT,
};

export const author2js: JsonapiDocument = {
  data: {
    id: AUTHOR_2_ID,
    type: AuthorType,
    attributes: {
      name       : AUTHOR_2_NAME,
      dob        : AUTHOR_2_BIRTH + 'T00:00:00.000Z',
      createdAt  : AUTHOR_2_CREATED,
      updatedAt  : AUTHOR_2_UPDATED,
      wordsCount : AUTHOR_2_WORDSCOUNT,
    },
  },
};

export const author1Wbooks = { ...author1, books: [bookFix.book1, bookFix.book2] };
export const author1jsWbooks = {
  data: {
    ...author1js.data,
    relationships: { artworks: { data: [bookFix.book1js.data, bookFix.book2js.data] }},
  },
  included: [bookFix.book1js.data, bookFix.book2js.data]
};
