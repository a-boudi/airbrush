import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResourceBase, Model, ServiceDecorator, Attribute, BelongsTo, JsonapiBaseService } from '../../src/public-api';

import { Author, Book, Paragraph } from './models.index';
import { ChapterType, AuthorType, BookType, ParagraphType } from './types.index';



@Model({ type: ChapterType })
export class Chapter extends ResourceBase {
  @Attribute()
  title: string;

  @Attribute()
  ordering: number;

  @Attribute()
  createdAt: Date;

  @Attribute()
  updatedAt: Date;

  @BelongsTo({ class: BookType })
  book: Book | Partial<Book>;

  @BelongsTo({ class: ParagraphType, sidepostable: true })
  firstParagraph: Paragraph | Partial<Paragraph>;

  @BelongsTo({ class: AuthorType, readonly: true })
  author: Author | Partial<Author>;
}

@Injectable()
@ServiceDecorator({
  model: () => Chapter,
})
export class ChapterService extends JsonapiBaseService<Chapter> {
  baseUrl = 'localhost:4200/api';
  className = ChapterService;

  constructor(http: HttpClient) { super(http); }
}
