import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResourceBase, Model, ServiceDecorator, Attribute, JsonapiBaseService, BelongsTo } from '../../src/public-api';

import { Author } from './models.index';
import { AwardType, AuthorType } from './types.index';


@Model({ type: AwardType })
export class Award extends ResourceBase {
  @Attribute()
  name: string;

  @Attribute()
  year: string;

  @BelongsTo({ class: AuthorType })
  author: Author | Partial<Author>;
}

@Injectable()
@ServiceDecorator({ model: () => Award })
export class AwardService extends JsonapiBaseService<Award> {
  baseUrl = 'localhost:4200/api';
  className = AwardService;

  constructor(http: HttpClient) { super(http); }
}
