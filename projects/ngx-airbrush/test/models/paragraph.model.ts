import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResourceBase, Model, ServiceDecorator, Attribute, BelongsTo, JsonapiBaseService } from '../../src/public-api';

import { Chapter } from './models.index';
import { ParagraphType, ChapterType } from './types.index';


@Model({ type: ParagraphType })
export class Paragraph extends ResourceBase {
  @Attribute()
  text: string;

  @Attribute()
  wordCount: number;

  @BelongsTo({ class: ChapterType })
  chapter: Chapter;
}

@Injectable()
@ServiceDecorator({
  model: () => Paragraph,
})
export class ParagraphService extends JsonapiBaseService<Paragraph> {
  baseUrl = 'localhost:4200/api';
  className = ParagraphService;

  constructor(http: HttpClient) { super(http); }
}
