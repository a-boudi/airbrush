import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResourceBase, Model, ServiceDecorator, Attribute, HasMany, BelongsTo, JsonapiBaseService } from '../../src/public-api';

import { Author, Chapter } from './models.index';
import { BookType, AuthorType, ChapterType } from './types.index';


@Model({ type: BookType })
export class Book extends ResourceBase {
  @Attribute()
  title: string;

  @Attribute()
  datePublished: Date;

  @Attribute()
  createdAt: Date;

  @Attribute()
  updatedAt: Date;

  @HasMany({ class: ChapterType, sidepostable: true })
  chapters: (Chapter | Partial<Chapter>)[];

  @BelongsTo({ class: ChapterType, sidepostable: true, backendName: 'first_chapter' })
  firstChapter: Chapter | Partial<Chapter>;

  @BelongsTo({ class: AuthorType })
  author: Author | Partial<Author>;
}

@Injectable()
@ServiceDecorator({
  model: () => Book,
  endpointUrl: 'books',
})
export class BookService extends JsonapiBaseService<Book> {
  baseUrl = 'localhost:4200/api';
  className = BookService;

  constructor(http: HttpClient) { super(http); }
}
