export const AuthorType    = 'authors';
export const AwardType     = 'awards';
export const BookType      = 'books';
export const ChapterType   = 'chapters';
export const ParagraphType = 'paragraphs';
