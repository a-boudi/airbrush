import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResourceBase, Model, ServiceDecorator, Attribute, HasMany, JsonapiBaseService } from '../../src/public-api';

import { Award, Book } from './models.index';
import { AuthorType, AwardType, BookType } from './types.index';


@Model({ type: AuthorType })
export class Author extends ResourceBase {
  @Attribute()
  name: string;

  @Attribute({
    backendName: 'dob'
  })
  dateOfBirth: Date;

  @Attribute()
  createdAt: Date;

  @Attribute()
  updatedAt: Date;

  @Attribute({ readonly: true })
  wordsCount: number;

  @HasMany({ class: BookType, sidepostable: true, backendName: 'artworks' })
  books: (Book | Partial<Book>)[];

  @HasMany({ class: BookType, readonly: true })
  bestSellers: (Book | Partial<Book>)[];

  @HasMany({ class: AwardType })
  awards: (Award | Partial<Award>)[];
}

@Injectable()
@ServiceDecorator({
  model: () => Author,
  endpointUrl: 'authors',
})
export class AuthorService extends JsonapiBaseService<Author> {
  baseUrl = 'localhost:4200/api';
  className = AuthorService;

  constructor(http: HttpClient) { super(http); }
}
