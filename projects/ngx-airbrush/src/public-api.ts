/*
 * Public API Surface of ngx-airbrush
 */

export * from './lib/ngx-airbrush.module';

// Decorators
export * from './lib/decorators/service.decorator';
export * from './lib/decorators/model.decorator';
export * from './lib/decorators/attribute.decorator';
export * from './lib/decorators/belongs-to.decorator';
export * from './lib/decorators/has-many.decorator';

// ResourceBase
export * from './lib/resource-base.model';

// JsonapiServiceBase
export * from './lib/jsonapi-base.service';

// Scope
export * from './lib/scope';
