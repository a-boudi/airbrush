import { ResourceBase } from './resource-base.model';
import { TypesStore } from './types-store';

export class Author extends ResourceBase { }
export class Painters extends ResourceBase { }


// tslint:disable: no-string-literal
describe('Types Store', () => {
  let store: TypesStore;

  beforeEach(() => store = TypesStore.getInstance());

  afterEach(() => TypesStore['instance'] = null);

  it('should be unique', () => {
    const store2 = TypesStore.getInstance();
    expect(store2).toBe(store);
  });

  it('should add a resource from class', () => {
    TypesStore.add('authors', Author);
    expect(store['_types']).toEqual({ authors: Author });
  });

  it('should add a resource from instance', () => {
    store.add('authors', Author);
    expect(store['_types']).toEqual({ authors: Author });
  });

  it('should find a resource from class', () => {
    store['_types']['authors'] = Author;
    expect(TypesStore.find('authors')).toEqual(Author);
  });

  it('should find a resource from instance', () => {
    store['_types']['authors'] = Author;
    expect(store.find('authors')).toEqual(Author);
  });

  it('should find a resource from class', () => {
    store['_types']['authors'] = Author;
    store['_types']['painters'] = Painters;
    expect(TypesStore.all()).toEqual({ authors: Author, painters: Painters });
  });

  it('should find a resource from instance', () => {
    store['_types']['authors'] = Author;
    store['_types']['painters'] = Painters;
    expect(store.all).toEqual({ authors: Author, painters: Painters });
  });
});
// tslint:enable: no-string-literal
