import { Author, Book, Chapter } from '../../test/models/models.index';
import { AuthorType } from '../../test/models/types.index';
import * as authFix from '../../test/fixtures/authors.fixture';
import * as chapFix from '../../test/fixtures/chapters.fixture';
import * as bookFix from '../../test/fixtures/books.fixture';


describe('JsonApiModel', () => {
  const authorData = authFix.author1;
  const authorjs = authFix.author1js;

  describe('constructor', () => {

    it('should be instantiated with attributes', () => {
      const author: Author = new Author(authorData);
      expect(author).toBeDefined();
      expect(author.id).toEqual(authorData.id);
      expect(author.name).toEqual(authorData.name);
      expect(author.dateOfBirth).toEqual(authorData.dateOfBirth);
    });

    it('should be instantiated without attributes', () => {
      const author: Author = new Author();
      expect(author).toBeDefined();
      expect(author.id).toBeUndefined();
      expect(author.dateOfBirth).toBeUndefined();
    });

    it('should not destroy if not a relationship', () => {
      const author = new Author();
      expect(author).toBeDefined();
      expect(author.destroy.bind(author)).toThrowError();
    });
  });

  describe('hasDirtyAttributes', () => {

    it('should be instantiated with attributes', () => {
      const author: Author = new Author(authorData);
      expect(author.hasDirtyAttributes).toBeFalsy();
    });

    it('should have dirty attributes after change', () => {
      const author: Author = new Author();
      expect(author.hasDirtyAttributes).toBeFalsy();
      author.name = 'Peter';
      expect(author.hasDirtyAttributes).toBeTruthy();
    });

    it('should reset dirty attributes', () => {
      const author: Author = new Author(authorData);
      author.name = 'Peter';
      author.rollbackAttributes();
      expect(author.hasDirtyAttributes).toBeFalsy();
      expect(author.name).toContain(authorData.name);
    });

  });

  describe('deserialize json document', () => {

    it('should be instantiated with attributes', () => {
      const author = Author.getModel<Author>(Author, authorjs);
      expect(author).toBeDefined();
      expect(author.id).toEqual(authorData.id);
      expect(author.name).toEqual(authorData.name);
      expect(author.dateOfBirth).toEqual(authorData.dateOfBirth);
    });

    it('should be instantiated with hasMany', () => {
      const author = Author.getModel<Author>(Author, authFix.author1jsWbooks);
      expect(author).toBeDefined();
      expect(author.books).toBeDefined();
      expect(author.books.length).toEqual(2);
      // FIXME: should be able to check equality between serialized author and author1
      // expect(author).toEqual(authFix.author1Wbooks);
    });

    it('should be instantiated with belongsTo', () => {
      const chap = chapFix.chapter1js;
      const book = bookFix.book1js;
      const auth = authFix.author1js;
      book.data.relationships = { author: auth };
      chap.data.relationships = { book };
      chap.included = [book.data, auth.data];
      const chapter = Chapter.getModel<Chapter>(Chapter, chap);
      expect(chapter).toBeDefined();
      expect(chapter.book).toBeDefined();
      expect(chapter.book.author).toBeDefined();
      // FIXME: should be able to check equality between serialized author and author1
      // expect(author).toEqual(authFix.author1Wbooks);
    });

    it('should convert backend property name to frontend property name', () => {
      const chap = { ...chapFix.chapter2js };
      const book = { ...bookFix.book2js };
      const auth = { ...authFix.author1js };
      book.data.relationships = { first_chapter: chap };
      auth.data.relationships = { artworks: { data: [book.data]} };
      auth.included = [book.data, chap.data];
      const author = Chapter.getModel<Author>(Author, auth);

      expect(author).toBeDefined();
      expect(author.books).toBeDefined();
      expect(author.artworks).toBeUndefined();
      expect(author.books[0].firstChapter).toBeDefined();
      expect(author.books[0].first_chapter).toBeUndefined();
    });

  });

  describe('serialize resource', () => {

    describe('attributes', () => {
      it('should serialize all', () => {
        const author = new Author({ ...authorData, id: null });
        const body = author.toJsonapi();

        expect(body.data.type).toBe(AuthorType);
        expect(Object.keys(body.data.attributes).length).toEqual(Object.keys(authorData).length - 2);
      });

      it('should serialize only dirty', () => {
        const author = new Author(authorData);
        author.name = 'test';
        const body = author.toJsonapi();

        expect(body.data.id).toEqual(authorData.id);
        expect(Object.keys(body.data.attributes).length).toEqual(1);
        expect(body.data.attributes.name).toEqual('test');
      });

      it('should use backend name', () => {
        const author = new Author(authorData);
        author.dateOfBirth = '1890-08-20' as any;
        const body = author.toJsonapi();

        expect(body.data.attributes.dob).toBeDefined();
      });

      it('should use serialized value', () => {
        const author = new Author(authorData);
        author.dateOfBirth = '1890-08-20' as any;
        const body = author.toJsonapi();

        expect(body.data.attributes.dob).toEqual('1890-08-20T00:00:00.000Z');
      });

      it('should define included when sidepostable', () => {
        const author = new Author({ id: authorData.id });
        const body = author.toJsonapi();

        expect(body.data.id).toEqual(authorData.id);
        expect(Object.keys(body.data.attributes).length).toEqual(0);
      });

      it('should not serialize wordsCount', () => {
        const author = new Author({ ...authorData, id: null });
        const body = author.toJsonapi();

        expect(body.data.attributes.wordsCount).toBeUndefined();
      });
    });

    describe('with a belongs to', () => {
      const chap = { ...chapFix.chapter1 };
      const book = { ...bookFix.book1 };
      const chapWbook = { ...chap, book };

      it('should add with update method', () => {
        const chapter = new Chapter(chap);
        chapter.book = book;

        const body = chapter.serialize() as any;
        expect(body.book.id).toEqual(book.id);
        expect(body.book.__method).toEqual('update');
      });

      it('should not serialize when untouched', () => {
        const chapter = new Chapter(chapWbook);

        const body = chapter.serialize() as any;
        expect(body.book).toBeUndefined();
      });

      it('should disassociate when setted to null', () => {
        const chapter = new Chapter(chapWbook);
        chapter.book = null;

        const body = chapter.serialize() as any;
        expect(body.book.id).toEqual(book.id);
        expect(body.book.__method).toEqual('disassociate');
      });

      it('should update association with update method', () => {
        const chapter = new Chapter(chapWbook);
        chapter.book = { id: '2' };

        const body = chapter.serialize() as any;
        expect(body.book.id).toEqual('2');
        expect(body.book.__method).toEqual('update');
      });

      it('should update when relation of relation is updated', () => {
        const paragraph = { id: '41', text: 'lorem Ipsum' };
        const chapter = { ...chapFix.chapter1, firstParagraph: paragraph };
        const bookHash = { ...bookFix.book1, firstChapter: chapter };
        const book1 = new Book(bookHash);
        book1.firstChapter.firstParagraph = { id: '42', text: 'lorem Ipsum' };

        const body = book1.serialize() as any;
        expect(body.first_chapter.id).toEqual(chapter.id);
        expect(body.first_chapter.__method).toEqual('update');

        expect(body.first_chapter.firstParagraph.id).toEqual('42');
        expect(body.first_chapter.firstParagraph.__method).toEqual('update');
      });

      it('should serialize when new resource', () => {
        const chapter = new Chapter();
        chapter.assign(chapWbook);

        const body = chapter.serialize() as any;

        expect(body.book.id).toEqual(chapWbook.book.id);
      });

      it('should not serialize author', () => {
        const chapter = new Chapter(chap);
        chapter.book = book;
        chapter.author = authorData;
        const body = chapter.toJsonapi();

        expect(body.data.relationships.book).toBeDefined();
        expect(body.data.relationships.author).toBeUndefined();
      });

    });

    describe('with has many', () => {
      const authorHash = { ...authFix.author1 };
      const book1Hash = { ...bookFix.book1, ...bookFix.book1js.data.attributes };
      const book2Hash = { ...bookFix.book2, ...bookFix.book2js.data.attributes };
      const authWbooks = { ...authorHash, books: [book1Hash, book2Hash] };
      let author: Author;

      beforeEach(() => {
        author = new Author(authWbooks);
      });

      it('should be undefined if not touched', () => {
        const body = author.serialize() as any;
        expect(body.artworks).toBeUndefined();
      });

      it('should add new relation', () => {
        author.books = [{ ...bookFix.book3 }];

        const body = author.serialize() as any;
        expect(body.artworks.length).toEqual(1);
        expect(body.artworks[0].__method).toEqual('update');
      });

      it('should disassociate', () => {
        const book1 = author.books[0];
        author.books = [{ id: book1.id, deleteRelation: true }];

        const body = author.serialize() as any;
        expect(body.artworks.length).toEqual(1);
        expect(book1.__method).toEqual('disassociate');
      });

      it('should update if attribute changed', () => {
        const book1 = author.books[0];
        author.books = [{ ...book1Hash, title: 'New Title' }];

        const body = author.serialize() as any;
        expect(body.artworks.length).toEqual(1);
        expect(book1.title).toEqual('New Title');
        expect(book1.__method).toEqual('update');
      });

      it('should update when relation of relation is updated', () => {
        const chapters = [{ ...chapFix.chapter1 }, { ...chapFix.chapter2 }];
        const books = [{ ...bookFix.book1, chapters }];

        author = new Author({ ...authorHash, books });
        author.books[0].chapters = [{ id: chapters[0].id, title: 'chapter 1' }];

        const body = author.serialize() as any;
        expect(body.artworks[0].id).toEqual(books[0].id);
        expect(body.artworks[0].__method).toEqual('update');

        expect(body.artworks[0].chapters.length).toEqual(1);
        expect(body.artworks[0].chapters[0].id).toEqual(chapters[0].id);
        expect(body.artworks[0].chapters[0].__method).toEqual('update');
      });

      it('should serilize all when new resource', () => {
        author = new Author();
        author.assign(authWbooks);

        const body = author.serialize() as any;

        expect(body.artworks.length).toEqual(2);
        expect(body.artworks.map(bk => bk.__method)).toEqual(['update', 'update']);

        const book1 = body.artworks.find(bk => bk.id === book1Hash.id);
        const book2 = body.artworks.find(bk => bk.id === book2Hash.id);

        Object.keys(book1Hash).forEach(key => expect(book1[key]).toEqual(book1Hash[key]));
        Object.keys(book2Hash).forEach(key => expect(book2[key]).toEqual(book2Hash[key]));
      });

      it('should not serialize bestSellers', () => {
        author.books = [{ ...bookFix.book3 }];
        author.bestSellers = [book1Hash];
        const body = author.toJsonapi();

        expect(body.data.relationships.artworks).toBeDefined();
        expect(body.data.relationships.bestSellers).toBeUndefined();
      });
    });
  });

  describe('serialize into jsonapi format', () => {

    describe('with a belongs to', () => {
      const chap = { ...chapFix.chapter1 };
      const book = { ...bookFix.book1 };
      const firstParagraph = { id: '41', text: 'lorem Ipsum' };

      it('should define only relationship when non sidepostable', () => {
        const chapter = new Chapter();
        chapter.assign({ ...chap, book });

        const body = chapter.toJsonapi();

        expect(body.data.relationships.book).toBeDefined();
        expect(body.included).toBeUndefined();
      });

      it('should define included when sidepostable', () => {
        const chapter = new Chapter();
        chapter.assign({ ...chap, firstParagraph });

        const body = chapter.toJsonapi();

        expect(body.data.relationships.firstParagraph).toBeDefined();
        expect(body.included.length).toEqual(1);
        expect(body.included[0].id).toEqual('41');
      });
    });

    describe('with has many', () => {
      const authorHash = { ...authFix.author1 };
      const book1 = { ...bookFix.book1, ...bookFix.book1js.data.attributes };
      const book2 = { ...bookFix.book2, ...bookFix.book2js.data.attributes };
      const award1 = { id: '51', name: 'Cobalt' };
      const award2 = { id: '52', name: 'Cadmium' };
      let author: Author;

      it('should define included when sidepostable', () => {
        author = new Author();
        author.assign({ ...authorHash, books: [book1, book2] });

        const body = author.toJsonapi();

        expect(body.data.relationships.artworks).toBeDefined();
        expect(body.included.length).toEqual(2);
      });

      it('should define included when sidepostable', () => {
        author = new Author();
        author.assign({ ...authorHash, awards: [award1, award2] });

        const body = author.toJsonapi();

        expect(body.data.relationships.awards).toBeDefined();
        expect(body.included).toBeUndefined();
      });
    });

  });

  describe('Hash resource', () => {
    const authorH = { ...authFix.author1 };
    const chapH = { ...chapFix.chapter1 };
    const bookH = { ...bookFix.book1 };

    it('should add with update method', () => {
      const book = new Book({...bookH, author: authorH, chapters: [chapH] });

      const hash = book.toHash();
      Object.keys(bookH).forEach(key => expect(hash[key]).toEqual(bookH[key]));
      Object.keys(authorH).forEach(key => expect(hash.author[key]).toEqual(authorH[key]));
      Object.keys(chapH).forEach(key => expect(hash.chapters[0][key]).toEqual(chapH[key]));
    });

  });

});
