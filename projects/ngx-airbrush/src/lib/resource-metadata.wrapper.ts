import { ResourceBase } from './resource-base.model';
import { AttributeMetadata, BelongsToMetadata, HasManyMetadata } from './decorators/symboles';


let getIncludedKeys: (model: ResourceBase, wrapper: ResourceMetadataWrapper, options?: { forBackend?: boolean }) => string[];

export class ResourceMetadataWrapper {

  constructor(private model: ResourceBase) {}

  public allKeys(options?: { defined?: boolean }): string[] {
    return [
      ...this.attributesKeys(options),
      ...this.relationshipKeys(options),
    ];
  }

  public relationshipKeys(options?: { defined?: boolean }): string[] {
    return [
      ...this.belongsToKeys(options),
      ...this.hasManyKeys(options)
    ];
  }

  public attributesKeys(options?: { defined?: boolean }): string[] {
    let keys = this.retrieveKeys(AttributeMetadata);
    if (options && options.defined) {
      keys = keys.filter(key => this.model[key] !== undefined);
      // FIXME: quick fix to delete null values when it's a create
      if (!this.model.id) { keys = keys.filter(key => this.model[key] !== null); }
    }
    return keys;
  }

  public belongsToKeys(options?: { defined?: boolean }): string[] {
    return this.retrieveKeys(BelongsToMetadata, options);
  }

  public hasManyKeys(options?: { defined?: boolean }): string[] {
    return this.retrieveKeys(HasManyMetadata, options);
  }

  public allMetadata(): any[] {
    return [
      ...this.attributesMetadata(),
      ...this.relationshipMetadata(),
    ];
  }

  public relationshipMetadata(): string[] {
    return [
      ...this.belongsToMetadata(),
      ...this.hasManyMetadata()
    ];
  }

  public attributesMetadata(): any[] {
    return this.retrieveMetadata(AttributeMetadata);
  }

  public belongsToMetadata(): any[] {
    return this.retrieveMetadata(BelongsToMetadata);
  }

  public hasManyMetadata(): any[] {
    return this.retrieveMetadata(HasManyMetadata);
  }

  public isSidepostable(propertyName: string) {
    let metadata: any[] = Reflect.getMetadata(BelongsToMetadata, this.model);
    let found = metadata && metadata.find(rel => rel.propertyName === propertyName);
    if (found) { return found.sidepostable; }

    metadata = Reflect.getMetadata(HasManyMetadata, this.model);
    found = metadata && metadata.find(rel => rel.propertyName === propertyName);
    if (found) { return found.sidepostable; }
  }

  public isWritable(propertyName: string) {
    const allMetadata = this.allMetadata();

    const found = allMetadata.find(pname => pname.propertyName === propertyName);
    if (found) { return !found.readonly; }
  }

  public backendPropName(propertyName: string | symbol): string {
    const allMetadata = this.allMetadata();

    const found = allMetadata.find(pname => pname.propertyName === propertyName);
    if (found) { return found.backendName; }
  }

  public frontendPropName(propertyName: string): string {
    const allMetadata = this.allMetadata();

    // search in backend
    let found = allMetadata.find(pname => pname.backendName === propertyName);
    if (found) { return found.propertyName; }
    // search in frontend
    found = allMetadata.find(pname => pname.propertyName === propertyName);
    if (found) { return propertyName; }
  }

  private retrieveKeys(symbol: any, options?: { defined?: boolean }) {
    this.model.modelSerialization = true;
    const metadata = this.retrieveMetadata(symbol);

    let keys = metadata.map(btm => btm.propertyName);
    if (options && options.defined) { keys = keys.filter(key => this.model[key]); }
    this.model.modelSerialization = false;
    return keys;
  }

  private retrieveMetadata(symbol: any): any[] {
    return Reflect.getMetadata(symbol, this.model) || [];
  }

  public getIncludedKeys(options?: { forBackend?: boolean }): string[] {
    options = (options || {}) && { forBackend: true, ...options };
    return getIncludedKeys(this.model, this, options);
  }
}

getIncludedKeys = (model: ResourceBase, wrapper: ResourceMetadataWrapper, options?: { forBackend?: boolean }): string[] => {
  const keyTranslator = options.forBackend ? 'backendPropName' : 'frontendPropName';
  let localKeys = wrapper.relationshipKeys({ defined: true }).filter(key => wrapper.isSidepostable(key));

  const childKeys: string[] = [];
  localKeys.forEach(key => {
    const resource = model[key];
    let cKeys = [];
    if (Array.isArray(resource)) {
      resource.forEach(elt => {
        const wrap = new ResourceMetadataWrapper(elt);
        const keys = getIncludedKeys(elt, wrap, options);
        cKeys.push(...keys);
      });
    } else {
      const wrap = new ResourceMetadataWrapper(resource);
      const keys = getIncludedKeys(resource, wrap, options);
      cKeys.push(...keys);
    }
    cKeys = cKeys.map(ckey => `${wrapper[keyTranslator](key)}.${ckey}`);
    // remove duplicates
    cKeys = cKeys.filter((item, index) => cKeys.indexOf(item) >= index);
    childKeys.push(...cKeys);
  });
  localKeys = localKeys.map(lkey => wrapper[keyTranslator](lkey));
  return [...localKeys, ...childKeys];
};
