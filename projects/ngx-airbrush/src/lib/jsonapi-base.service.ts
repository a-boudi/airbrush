import { HttpClient, HttpResponse, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';

import { ServiceDecoratorOptions, ModelDecoratorOptions } from './decorators/option.interfaces';
import { ServiceMetadata, ModelMetadata } from '../lib/decorators/symboles';
import { ResourceBase } from './resource-base.model';
import { JsonapiResponse } from './interfaces/jsonapi.interface';
import { Scope } from './scope';
import { SortScope, FieldScope, FilterScope, StatsScope, IncludeScope } from './interfaces/query.interface';


export interface Response extends HttpResponse<JsonapiResponse> {}

export abstract class JsonapiBaseService<T extends ResourceBase> {
  abstract baseUrl: string;
  abstract className: any;


  constructor(protected http: HttpClient) {}

  public all(options?: { params?: HttpParams }): Observable<T[]>;
  public all(options: { params?: HttpParams, metadata: true }): Observable<{ data: T[], meta: any }>;

  public all(options?: { params?: HttpParams, metadata?: boolean }): Observable<T[]> | Observable<{ data: T[], meta: any }> {
    const url = this.buildUrl();
    options = options || {};

    const params = options.params;
    const request = this.http.get(url, { params, observe: 'response' }).pipe(
      map((res: Response) => this.successful(res)),
    );

    if (options.metadata) {
      return request.pipe(
        map((res: Response) => (({ data: this.extract(res), meta: this.extractMeta(res) }))),
        catchError((res: any) => this.handleError(res))
      );
    } // else
    return request.pipe(
      map((res: Response) => this.extract(res)),
      catchError((res: any) => this.handleError(res))
    );
  }

  public find(id: string, params?: HttpParams): Observable<T> {
    const url = this.buildUrl(id);

    return this.http.get(url, { params, observe: 'response' }).pipe(
      map((res: Response) => this.successful(res)),
      map((res: Response) => this.extract(res)),
      catchError((res: any) => this.handleError(res))
    );
  }

  public save(model: T): Observable<T> {
    const url = this.buildUrl(model.id);

    const body = model.toJsonapi();

    let httpCall: Observable<Response>;
    if (model.id) {
      httpCall = this.http.put<object>(url, body, { observe: 'response' }) as Observable<Response>;
    } else {
      httpCall = this.http.post<object>(url, body, { observe: 'response' }) as Observable<Response>;
    }

    return httpCall.pipe(
      map((res: Response) => this.successful(res)),
      map((res: Response) => this.extract(res)),
      catchError((res: any) => this.handleError(res))
    );
  }

  public delete(id: string): Observable<any> {
    const url = this.buildUrl(id);

    return this.http.delete(url, { observe: 'response' }).pipe(
      map((res: Response) => this.successful(res)),
      catchError((res: HttpErrorResponse) => this.handleError(res))
    );
  }

  protected extractMeta(response: Response) {
    const body = response.body;
    const meta = body.meta;
    return meta;
  }

  protected extract(response: Response): T | T[] {
    const body = response.body;
    const records = body.data;

    if (Array.isArray(records)) {
      const models: T[] = [];
      records.forEach((data: any) => {
        const model = this.deserializeModel({ data, included: body.included });
        models.push(model);
      });
      return models;
    } else {
      const model = this.deserializeModel(body);
      return model;
    }
  }

  protected deserializeModel(data: any): T {
    const modelType = this.getModelType();
    return ResourceBase.getModel<T>(modelType, data);
  }

  protected handleError(error: any): Observable<any> {
    return throwError(error);
  }

  protected buildUrl(id?: string): string {
    const serviceConfig = this.getServiceConfig();
    const modelConfig = this.getModelConfig();

    const baseUrl = this.baseUrl;
    const apiVersion = serviceConfig.apiVersion;
    const endpointUrl: string = serviceConfig.endpointUrl || modelConfig.type;

    const url: string = [baseUrl, apiVersion, endpointUrl, id].filter(itm => !!itm).join('/');

    return url;
  }

  private getModelType() {
    const serviceConfig = this.getServiceConfig();
    return serviceConfig.model();
  }

  public getModelConfig(): ModelDecoratorOptions {
    return Reflect.getMetadata(ModelMetadata, this.getModelType());
  }

  public getServiceConfig(): ServiceDecoratorOptions {
    return Reflect.getMetadata(ServiceMetadata, this.className);
  }

  protected successful(response: Response): Response {
    const code = response.status;
    if ( code < 400 ) { return response; }
    throw(response);
  }

  public page(pageNumber: number): Scope<T> {
    const scope = new Scope(this);
    return scope.page(pageNumber);
  }

  public per(size: number): Scope<T> {
    const scope = new Scope(this);
    return scope.per(size);
  }

  public where(clause: FilterScope): Scope<T> {
    const scope = new Scope(this);
    return scope.where(clause);
  }

  public stats(clause: StatsScope): Scope<T> {
    const scope = new Scope(this);
    return scope.stats(clause);
  }

  public order(clause: SortScope | string): Scope<T> {
    const scope = new Scope(this);
    return scope.order(clause);
  }

  public select(clause: FieldScope | string | string[]): Scope<T> {
    const scope = new Scope(this);
    return scope.select(clause);
  }

  public includes(clause: IncludeScope): Scope<T> {
    const scope = new Scope(this);
    return scope.includes(clause);
  }
}
