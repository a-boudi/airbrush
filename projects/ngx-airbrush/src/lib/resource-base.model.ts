import { JsonapiConverter } from './converters/jsonapi.converter';
import { JsonapiDocument, JsonapiResourceRequest, JsonapiResourceMethod } from './interfaces/jsonapi.interface';
import { ModelDecoratorOptions } from './decorators/option.interfaces';
import { ModelMetadata, AttributeMetadata } from './decorators/symboles';
import { ResourceSerializer } from './resource.serializer';
import { ResourceMetadataWrapper } from './resource-metadata.wrapper';


type methodAction = JsonapiResourceMethod | 'untouched';

export abstract class ResourceBase {
  id: string;
  public modelInitialization = false;
  public modelSerialization = false;

// tslint:disable: variable-name
  private __method: methodAction;
  private __isRelation = false;
  private __mdwrapper: ResourceMetadataWrapper;
  private get __wrapper(): ResourceMetadataWrapper {
    if (!this.__mdwrapper) { this.__mdwrapper = new ResourceMetadataWrapper(this); }
    return this.__mdwrapper;
  }
  private __resSerializer: ResourceSerializer;
  private get __serializer(): ResourceSerializer {
    if (!this.__resSerializer) { this.__resSerializer = new ResourceSerializer(this); }
    return this.__resSerializer;
  }
// tslint:enable: variable-name

  [key: string]: any;

  static getModel<T extends ResourceBase>(constructor: any, data?: JsonapiDocument): T {
    const ret = new JsonapiConverter().deserialize(data);
    return new constructor(ret);
  }

  static getAsRelationship<T extends ResourceBase>(constructor: any, data: ResourceBase, options?: any): T {
    if (!data) { return null; }
    options = { method: 'untouched', modelInit: false, ...options };
    let model: T;
    if (options.modelInit) {
      model = new constructor(data);
    } else {
      model = new constructor();
      model.id = data.id;
      model.assign(data);
    }
    model.__isRelation = true;
    model.__method = options.method;
    return model;
  }

  constructor(data?: Partial<ResourceBase>) {
    if (!data) { return; }
    this.modelInitialization = true;
    this.id = data.id;
    this.assign(data);
    this.modelInitialization = false;
  }

  public isModelInitialization(): boolean {
    return this.modelInitialization;
  }

  get modelOptions(): ModelDecoratorOptions {
    return Reflect.getMetadata(ModelMetadata, this.constructor);
  }

  get type(): string {
    return this.modelOptions.type;
  }

  get hasBeenTouched() {
    const touched = this.__isRelation && this.__method !== 'untouched';
    return this.hasDirtyAttributes || this.hasDirtyBelongsTos || this.hasDirtyHasManys || touched;
  }

  get hasDirtyAttributes() {
    const attributesMetadata: any = this[AttributeMetadata];
    for (const propertyName in attributesMetadata) {
      if (attributesMetadata.hasOwnProperty(propertyName) && attributesMetadata[propertyName].hasDirtyAttributes) {
        return true;
      }
    }
    return false;
  }

  get hasDirtyBelongsTos(): boolean {
    const keys = this.__wrapper.belongsToKeys({ defined: true });

    let touched = false;
    keys.forEach(key => {
      touched = touched || this[key].hasBeenTouched;
    });
    return touched;
  }

  get hasDirtyHasManys(): boolean {
    const keys = this.__wrapper.hasManyKeys({ defined: true });

    let touched = false;
    keys.forEach(key => {
      touched = touched || this[key].map(elt => elt.hasBeenTouched).reduce((res, bool) => res || bool, false);
    });
    return touched;
  }

  get isRelation() {
    return this.__isRelation;
  }

  get relationMethod() {
    if (this.__method !== 'untouched') { return this.__method; }
    if (!this.hasBeenTouched) { return 'untouched'; }
    if (this.id) { return 'update'; }
    return 'create';
  }

  public rollbackAttributes(): void {
    const attributesMetadata: any = this[AttributeMetadata];
    for (const propertyName in attributesMetadata) {
      if (attributesMetadata.hasOwnProperty(propertyName) && attributesMetadata[propertyName].hasDirtyAttributes) {
        this[propertyName] = attributesMetadata[propertyName].oldValue;
      }
    }
  }

  public assign(data) {
    const clone = this.translateKeys(data);
    Object.assign(this, clone);
    if (this.__isRelation) {
      this.__method = this.relationMethod;
    }
  }

  public disassociate() {
    this.__method = 'disassociate';
  }

  public destroy() {
    if (this.isRelation) {
      this.__method = 'destroy';
    } else {
      throw new Error('Destroying resource is not supported! Use the service instead.');
    }
  }

  public toJsonapi(): JsonapiResourceRequest {
    return this.__serializer.toJsonapi();
  }

  public toHash(): { [attrName: string]: any; } {
    return this.__serializer.toHash();
  }

  public sideload() {
    const method = this.relationMethod;
    if (method === 'create' || method === 'untouched') { return; }

    const type = this.modelOptions.type;

    return {
      id: this.id,
      type,
      __method: method,
    };
  }

  public serialize() {
    return this.__serializer.serialize();
  }

  private translateKeys(obj: any) {
    if (obj.toHash) { obj = obj.toHash(); }

    const filtred = {};
    for (const oldkey in obj) {
      if (obj.hasOwnProperty(oldkey)) {
        const newkey = this.__wrapper.frontendPropName(oldkey.toString());
        if (newkey) { filtred[newkey] = obj[oldkey]; }
      }
    }
    return filtred;
  }
}
