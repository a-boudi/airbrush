import { ModelDecoratorOptions } from './option.interfaces';
import { ModelMetadata } from './symboles';
import { TypesStore } from '../types-store';


export function Model(options: ModelDecoratorOptions) {
  return (target: any) => {
    Reflect.defineMetadata(ModelMetadata, options, target);
    TypesStore.add(options.type, target);
  };
}
