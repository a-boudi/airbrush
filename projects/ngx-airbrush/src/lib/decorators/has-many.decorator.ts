import { RelationshipDecoratorOptions } from './option.interfaces';
import { HasManyMetadata } from './symboles';
import { ResourceBase } from '../resource-base.model';
import { TypesStore } from '../types-store';


export function HasMany(options: RelationshipDecoratorOptions = {}) {
    return (target: any, propertyName: string) => {

      const setupMetadata = () => {
        const hasManys = Reflect.getMetadata(HasManyMetadata, target) || [];

        hasManys.push({
          propertyName,
          backendName: options.backendName || propertyName,
          readonly: !!options.readonly,
          sidepostable: !!options.sidepostable,
        });

        Reflect.defineMetadata(HasManyMetadata, hasManys, target);
      };

      const updateSideload = (propClass: any, oldValues: any[], newValues: any[]) => {
        if (!newValues) { return oldValues; }

        const notConcerned = oldValues && oldValues.filter(oldVal => (newValues.findIndex(newVal => oldVal.id === newVal.id) === -1));
        const result = notConcerned || [];
        newValues.forEach(newVal => {
          // no ids: can't create a resource. This is a sideload!
          if (!newVal.id) { return; }
          const found = oldValues && oldValues.find(oldVal => oldVal.id === newVal.id);
          // no old or id not found: create new relationship
          if (!found) {
            result.push(ResourceBase.getAsRelationship<any>(propClass, newVal, {method: 'update'}));
            return;
          }
          // mark for disassociation
          if (newVal.deleteRelation) { found.disassociate(); }
          // keep existing relation
          result.push(found);
        });

        return result;
      };

      const updateSidepost = (propClass: any, oldValues: any[], newValues: any[]) => {
        if (!newValues) { return oldValues; }

        const notConcerned = oldValues && oldValues.filter(oldVal => (newValues.findIndex(newVal => oldVal.id === newVal.id) === -1));
        const result = notConcerned || [];
        newValues.forEach(newVal => {
          const newHaveId = !!(newVal && newVal.id);
          const newHaveVal = !!(newVal && Object.keys(newVal).filter(key => key !== 'id' && key !== 'type').length > 0);

          // new don't have id and value: do nothing!
          if (!newHaveId && !newHaveVal) { return; }
          // new don't have id but have value: create new
          if (!newHaveId) {
            result.push(ResourceBase.getAsRelationship<any>(propClass, newVal, {method: 'create'}));
            return;
          }
          const found = oldValues && oldValues.find(oldVal => oldVal.id === newVal.id);
          // no old or id not found: create new relationship
          if (!found) {
            result.push(ResourceBase.getAsRelationship<any>(propClass, newVal, {method: 'update'}));
            return;
          }
          // mark for disassociation
          if (newVal.deleteRelation || newVal.destroyRelation) {
            newVal.destroyRelation ? found.destroy() : found.disassociate();
            result.push(found);
            return;
          }
          // keep existing relation
          found.assign(newVal);
          result.push(found);
        });

        return result;
      };

      const getter = function() {
        if (!this[`_${propertyName}`] && !this.modelSerialization) { this[`_${propertyName}`] = []; }
        return this[`_${propertyName}`];
      };

      const setter = function(newVal: any) {
        // const propClass = options.class();
        const propClass = TypesStore.find(options.class);

        if (this.isModelInitialization()) {
          this[`_${propertyName}`] = newVal.map(elt => ResourceBase.getAsRelationship<any>(propClass, elt, {modelInit: true}));
        } else {
          const oldVal = this[`_${propertyName}`];
          this[`_${propertyName}`] = options.sidepostable ?
                                     updateSidepost(propClass, oldVal, newVal) :
                                     updateSideload(propClass, oldVal, newVal);
        }
      };

      if (delete target[propertyName]) {
        setupMetadata();
        Object.defineProperty(target, propertyName, {
          get: getter,
          set: setter,
          enumerable: true,
          configurable: true
        });
      }
    };
  }
