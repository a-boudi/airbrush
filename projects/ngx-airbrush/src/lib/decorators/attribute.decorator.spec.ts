import { ResourceBase, Model, Attribute } from '../../public-api';
import { PropertyConverter } from '../interfaces/property-converter.interface';
import { AttributeMetadata } from './symboles';
import { ResourceMetadataWrapper } from '../resource-metadata.wrapper';


class TimestampConverter implements PropertyConverter {
  deserialize(value: number) {
    if (!value) { return null; }
    // tslint:disable-next-line: no-use-before-declare
    return new Timestamp(value);
  }

  serialize(value: Timestamp) {
    if (!value) { return null; }
    const timestamp = value.date.getTime();
    return timestamp;
  }

  areEquals(oldValue: Timestamp, newValue: Timestamp): boolean {
    if (!oldValue && !newValue) { return true; }
    if (!oldValue || !newValue) { return false; }
    const oldVal = oldValue.date.getTime();
    const newVal = newValue.date.getTime();
    return oldVal === newVal;
  }
}

class Timestamp {
  date: Date;

  constructor(value: number) {
    this.date = new Date(value * 1000);
  }

  deserialize(value: number) {
    return new TimestampConverter().deserialize(value);
  }

  serialize(value: Timestamp) {
    return new TimestampConverter().serialize(value);
  }
}

@Model({ type: 'authors' })
export class Author extends ResourceBase {
  @Attribute()
  name: string;

  @Attribute({ backendName: 'dob' })
  dateOfBirth: Date;

  @Attribute({ converter: new TimestampConverter(), readonly: true })
  createdAt: Timestamp;

  @Attribute({ readonly: true })
  updatedAt: Timestamp;
}


describe('Attribute decorator', () => {
  let author: Author;
  const authorHash = {
    id: '1',
    name: 'Lovecraft',
    dateOfBirth: '1892-01-03',
    createdAt: 1585239174,
    updatedAt: 1585558897,
  };

  beforeEach(() => {
    author = new Author(authorHash);
  });

  it('should be instantiated', () => {
    expect(author).toBeDefined();
    expect(author.id).toEqual(authorHash.id);
    expect(author.name).toEqual(authorHash.name);
    expect(author.dateOfBirth).toEqual(new Date(authorHash.dateOfBirth));
    expect(author.createdAt).toEqual(new Timestamp(authorHash.createdAt));
    expect(author.updatedAt).toEqual(new Timestamp(authorHash.updatedAt));
  });

  it('should mark as dirty', () => {
    expect(author[AttributeMetadata].name.hasDirtyAttributes).toBeFalsy();
    author.name = 'Tolkien' as any;
    expect(author[AttributeMetadata].name.hasDirtyAttributes).toBeTruthy();

    expect(author[AttributeMetadata].dateOfBirth.hasDirtyAttributes).toBeFalsy();
    author.dateOfBirth = '1890-08-20' as any;
    expect(author[AttributeMetadata].dateOfBirth.hasDirtyAttributes).toBeTruthy();

    expect(author[AttributeMetadata].createdAt.hasDirtyAttributes).toBeFalsy();
    author.createdAt = 1585239175 as any;
    expect(author[AttributeMetadata].createdAt.hasDirtyAttributes).toBeTruthy();

    expect(author[AttributeMetadata].updatedAt.hasDirtyAttributes).toBeFalsy();
    author.updatedAt = 1585558898 as any;
    expect(author[AttributeMetadata].updatedAt.hasDirtyAttributes).toBeTruthy();
  });

  it('should not mark dirty if same', () => {
    expect(author[AttributeMetadata].name.hasDirtyAttributes).toBeFalsy();
    author.name = 'Tolkien' as any;
    author.name = 'Lovecraft' as any;
    expect(author[AttributeMetadata].name.hasDirtyAttributes).toBeFalsy();

    expect(author[AttributeMetadata].dateOfBirth.hasDirtyAttributes).toBeFalsy();
    author.dateOfBirth = '1890-08-20' as any;
    author.dateOfBirth = '1892-01-03' as any;
    expect(author[AttributeMetadata].dateOfBirth.hasDirtyAttributes).toBeFalsy();

    expect(author[AttributeMetadata].createdAt.hasDirtyAttributes).toBeFalsy();
    author.createdAt = 1585239175 as any;
    author.createdAt = 1585239174 as any;
    expect(author[AttributeMetadata].createdAt.hasDirtyAttributes).toBeFalsy();

    expect(author[AttributeMetadata].updatedAt.hasDirtyAttributes).toBeFalsy();
    author.createdAt = 1585558898 as any;
    author.updatedAt = 1585558897 as any;
    expect(author[AttributeMetadata].updatedAt.hasDirtyAttributes).toBeFalsy();
  });

  it('should set to null if cannot deserialize', () => {
    expect(author[AttributeMetadata].dateOfBirth.hasDirtyAttributes).toBeFalsy();
    author.dateOfBirth = '' as any;
    expect(author[AttributeMetadata].dateOfBirth.newValue).toBeNull();

    expect(author[AttributeMetadata].createdAt.hasDirtyAttributes).toBeFalsy();
    author.createdAt = 0 as any;
    expect(author[AttributeMetadata].createdAt.newValue).toBeNull();

    expect(author[AttributeMetadata].updatedAt.hasDirtyAttributes).toBeFalsy();
    author.updatedAt = null;
    expect(author[AttributeMetadata].updatedAt.newValue).toBeNull();
  });

  it('should keep old intact', () => {
    expect(author[AttributeMetadata].dateOfBirth.hasDirtyAttributes).toBeFalsy();
    author.dateOfBirth = '1891-01-03' as any;
    expect(author[AttributeMetadata].dateOfBirth.newValue).toEqual(new Date('1891-01-03'));
    expect(author[AttributeMetadata].dateOfBirth.oldValue).toEqual(new Date('1892-01-03'));

    author.dateOfBirth = '1893-01-05' as any;
    expect(author[AttributeMetadata].dateOfBirth.newValue).toEqual(new Date('1893-01-05'));
    expect(author[AttributeMetadata].dateOfBirth.oldValue).toEqual(new Date('1892-01-03'));
  });

  it('should set be writable unless readonly is true', () => {
    const mdw = new ResourceMetadataWrapper(author);
    expect(mdw.isWritable('name')).toBeTruthy();
    expect(mdw.isWritable('dateOfBirth')).toBeTruthy();
    expect(mdw.isWritable('createdAt')).toBeFalsy();
    expect(mdw.isWritable('updatedAt')).toBeFalsy();
  });
});
