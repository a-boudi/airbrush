export const ServiceMetadata = Symbol('ServiceMetadata') as any;
export const ModelMetadata = Symbol('ModelMetadata') as any;
export const AttributeMetadata = Symbol('AttributeMetadata') as any;
export const BelongsToMetadata = Symbol('BelongsToMetadata') as any;
export const HasManyMetadata = Symbol('HasManyMetadata') as any;
