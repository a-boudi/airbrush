import { ServiceDecoratorOptions } from './option.interfaces';
import { ServiceMetadata } from './symboles';

export function ServiceDecorator(options: ServiceDecoratorOptions) {
    return (target: any) => {
      Reflect.defineMetadata(ServiceMetadata, options, target);
    };
  }
