import { ResourceBase, Model, Attribute, BelongsTo } from '../../public-api';
import { ResourceMetadataWrapper } from '../resource-metadata.wrapper';

@Model({ type: 'authors' })
export class Author extends ResourceBase {
  @Attribute()
  name: string;
}

@Model({ type: 'editors' })
export class Editor extends ResourceBase {
  @Attribute()
  name: string;
}

@Model({ type: 'covers' })
export class Cover extends ResourceBase {
  @Attribute()
  color: string;

  @BelongsTo({ class: ['authors', 'editors'] })
  preface: Author | Editor;
}

@Model({ type: 'books' })
export class Book extends ResourceBase {
  @Attribute()
  title: string;

  @BelongsTo({ class: 'authors' })
  author: Author | Partial<Author>;

  @BelongsTo({ class: 'covers', sidepostable: true })
  cover: Cover | Partial<Cover>;

  @BelongsTo({ class: 'authors', readonly: true })
  coverArtist: Author | Partial<Author>;
}


describe('BelongsTo decorator', () => {
  const authorHash = { id: '1', name: 'Lovecraft' };
  const coverHash = { id: '4', color: 'Red' };
  const bookHash = { id: '7', title: 'The Shadow', author: authorHash, cover: coverHash };
  let book: Book;

  beforeEach(() => {
    book = new Book(bookHash);
  });

  it('should be instantiated with relations', () => {
    expect(book).toBeDefined();
    expect(book.id).toEqual(bookHash.id);
    expect(book.author.name).toEqual(authorHash.name);
    expect(book.cover.color).toEqual(coverHash.color);
  });

  it('should initialize as untouched', () => {
    expect(book.author.__method).toEqual('untouched');
    expect(book.cover.__method).toEqual('untouched');
  });

  it('should set be writable unless readonly is true', () => {
    const mdw = new ResourceMetadataWrapper(book);
    expect(mdw.isWritable('cover')).toBeTruthy();
    expect(mdw.isWritable('coverArtist')).toBeFalsy();
  });

  describe('Sideload', () => {
    it('should be set to update if belongsTo changed', () => {
      book.author = { id: '2', name: 'Tolkien' };
      expect(book.author.__method).toEqual('update');
    });

    it('should be untouched if id is same', () => {
      book.author = { ...authorHash, name: 'Tolkien' };
      expect(book.author.__method).toEqual('untouched');
    });

    it('should be set to disassociate implicitely', () => {
      book.author = null;
      expect(book.author.__method).toEqual('disassociate');
    });

    it('should be set to disassociate explicitely', () => {
      book.author = { ...authorHash, deleteRelation: true };
      expect(book.author.__method).toEqual('disassociate');
    });

    it('should not be set to destroy', () => {
      book.author = { ...authorHash, destroyRelation: true };
      expect(book.author.__method).toEqual('untouched');
    });

    it('should be set to null if it was empty', () => {
      const hash = { ...bookHash };
      delete hash.author;
      const book2 = new Book(hash);
      book2.modelSerialization = true;

      expect(book2.author).toBeFalsy();
      book2.author = null;
      expect(book2.author).toBeNull();
    });

    it('should get empty model if it was null', () => {
      const hash = { ...bookHash };
      delete hash.author;
      const book2 = new Book(hash);
      expect(book2.author).toBeTruthy();
      expect(book2.author.__method).toEqual('untouched');
    });
  });

  describe('Sidepost', () => {
    it('should be set to create', () => {
      book.cover = { color: 'Yellow' };
      expect(book.cover.__method).toEqual('create');
    });

    it('should be set to update if belongsTo changed', () => {
      book.cover = { id: '8', color: 'Blue' };
      expect(book.cover.__method).toEqual('update');
    });

    it('should be set to update if attribute changed', () => {
      book.cover = { ...coverHash, color: 'Green' };
      expect(book.cover.__method).toEqual('update');
    });

    it('should be untouched if new equals old', () => {
      book.cover = coverHash;
      expect(book.cover.__method).toEqual('untouched');
    });

    it('should be set to disassociate implicitely', () => {
      book.cover = null;
      expect(book.cover.__method).toEqual('disassociate');
    });

    it('should be set to disassociate explicitely', () => {
      book.cover = { ...coverHash, deleteRelation: true };
      expect(book.cover.__method).toEqual('disassociate');
    });

    it('should be set to destroy', () => {
      book.cover = { ...coverHash, destroyRelation: true };
      expect(book.cover.__method).toEqual('destroy');
    });

    it('should prioritize destroy over disassociate', () => {
      book.cover = { ...coverHash, deleteRelation: true, destroyRelation: true };
      expect(book.cover.__method).toEqual('destroy');
    });

    it('should be set to null if it was empty', () => {
      const hash = { ...bookHash };
      delete hash.cover;
      const book2 = new Book(hash);
      book2.modelSerialization = true;

      expect(book2.cover).toBeFalsy();
      book2.cover = null;
      expect(book2.cover).toBeNull();
    });

    it('should get empty model if it was null', () => {
      const hash = { ...bookHash };
      delete hash.cover;
      const book2 = new Book(hash);

      expect(book2.cover).toBeTruthy();
      expect(book2.cover.__method).toEqual('untouched');
    });
  });

  describe('Polymorphism', () => {
    let cover: Cover;
    const editorHash = { id: '41', name: 'JP', type: 'editors' };

    beforeEach(() => {
      cover = new Cover({ coverHash });
    });

    it('should be able to set both type with model', () => {
      cover.preface = new Editor(editorHash);
      expect(cover.preface.modelOptions.type).toEqual('editors');
      cover.preface = new Author({ ...authorHash, type: 'authors' });
      expect(cover.preface.modelOptions.type).toEqual('authors');
    });

    it('should be able to set both type with hash', () => {
      cover.preface = editorHash as any;
      expect(cover.preface.modelOptions.type).toEqual('editors');
      cover.preface = { ...authorHash, type: 'authors' } as any;
      expect(cover.preface.modelOptions.type).toEqual('authors');
    });

    it('should throw error if type is not recognized', () => {
      let func = () => { cover.preface = { ...editorHash, type: ''} as any; };
      expect(func).toThrowError();

      func = () => { cover.preface = { ...authorHash, type: 'type'} as any; };
      expect(func).toThrowError();
    });

    it('should update attribute when different type', () => {
      cover.preface = { ...editorHash, id: 'same' } as any;
      expect(cover.preface.modelOptions.type).toEqual('editors');
      cover.preface = { ...authorHash, id: 'same', type: 'authors' } as any;
      expect(cover.preface.modelOptions.type).toEqual('authors');
    });
  });
});
