import { AttributeDecoratorOptions } from './option.interfaces';
import { AttributeMetadata } from './symboles';
import { DateConverter } from '../converters/date.conveter';
import { PropertyConverter } from '../interfaces/property-converter.interface';


export function Attribute(options: AttributeDecoratorOptions = {}): PropertyDecorator {
  return (target: any, propertyName: string) => {

    const converter = (propType: any): any => {
      let attrConverter: PropertyConverter;

      if (options.converter) {
        attrConverter = options.converter;
      } else if (propType === Date) {
        attrConverter = new DateConverter();
      } else {
        const datatype = new propType();

        if (datatype.serialize && datatype.deserialize) {
          attrConverter = datatype;
        }
      }
      return attrConverter;
    };

    const convert = (propType: any, value: any, serialize = false): any => {
      const attrConverter = converter(propType);

      if (attrConverter) {
        if (serialize) { return attrConverter.serialize(value); }
        return attrConverter.deserialize(value);
      } // else
      return value;
    };

    const areEquals = (propType: any, oldValue: any, newValue: any): boolean => {
      const attrConverter = converter(propType);

      if (attrConverter) {
        if (attrConverter.areEquals) { return attrConverter.areEquals(oldValue, newValue); }
        // else
        const newVal = attrConverter.serialize(newValue);
        const oldVal = attrConverter.serialize(oldValue);
        return newVal === oldVal;
      } // else
      return oldValue === newValue;
    };

    const setupMetadata = () => {
      const attributes = Reflect.getMetadata(AttributeMetadata, target) || [];

      attributes.push({
        propertyName,
        backendName: options.backendName || propertyName,
        readonly: !!options.readonly,
        converter: options.converter,
      });

      Reflect.defineMetadata(AttributeMetadata, attributes, target);
    };

    const setMetadata = (instance: any, oldValue: any, newValue: any) => {
      const propType = Reflect.getMetadata('design:type', target, propertyName);

      instance[AttributeMetadata] = instance[AttributeMetadata] || {};
      instance[AttributeMetadata][propertyName] = {
        newValue,
        oldValue,
        backendName: options.backendName || propertyName,
        hasDirtyAttributes: !areEquals(propType, oldValue, newValue),
        serializedValue: convert(propType, newValue, true),
      };
    };

    const getter = function() {
      return this[`_${propertyName}`];
    };

    const setter = function(newVal: any) {
      const propType = Reflect.getMetadata('design:type', target, propertyName);
      const serialized = convert(propType, newVal);

      let oldValue = null;
      if (this.isModelInitialization() && this.id) {
        oldValue = serialized;
      } else {
        if (this[AttributeMetadata] && this[AttributeMetadata][propertyName]) {
          oldValue = this[AttributeMetadata][propertyName].oldValue;
        }
      }

      this[`_${propertyName}`] = serialized;
      setMetadata(this, oldValue, serialized);
    };

    if (delete target[propertyName]) {
      setupMetadata();
      Object.defineProperty(target, propertyName, {
        get: getter,
        set: setter,
        enumerable: true,
        configurable: true
      });
    }
  };
}
