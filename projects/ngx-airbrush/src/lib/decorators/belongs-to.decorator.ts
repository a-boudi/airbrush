import { RelationshipDecoratorOptions } from './option.interfaces';
import { BelongsToMetadata } from './symboles';
import { ResourceBase } from '../resource-base.model';
import { TypesStore } from '../types-store';


export function BelongsTo(options: RelationshipDecoratorOptions = {}) {
    return (target: any, propertyName: string) => {

      const setupMetadata = () => {
        const belongsTos = Reflect.getMetadata(BelongsToMetadata, target) || [];

        belongsTos.push({
          propertyName,
          backendName: options.backendName || propertyName,
          readonly: !!options.readonly,
          sidepostable: !!options.sidepostable,
        });

        Reflect.defineMetadata(BelongsToMetadata, belongsTos, target);
      };

      const getClass = (fromVal?: any) => {
        // const klass = options.class();
        if (!Array.isArray(options.class)) {
          return TypesStore.find(options.class);
        }
        const klasses = options.class.map(klass => TypesStore.find(klass));

        if (!fromVal) { return klasses[0]; }
        const type = fromVal.type;
        if (!type) { throw new Error('Object should be a subclass of ResourceBase or should have a type attribute!'); }

        const prop = klasses.find(k => {
          const obj = new k();
          return obj.type === type;
        });
        if (!prop) { throw new Error('Object should be a subclass of ResourceBase or should have a type attribute!'); }
        return prop;
      };

      const updateSideload = (propClass: any, oldValue: any, newValue: any) => {
        const noOldId = !(oldValue && oldValue.id);
        const noNewId = !(newValue && newValue.id);
        const noIds = noOldId && noNewId;
        const eqIds = noOldId || noNewId ? false : oldValue.id === newValue.id;

        // no ids: can't create a resource. This is a sideload!
        if (noIds) { return null; }
        // new don't exist: disassociate old from the resource
        if (noNewId || newValue.deleteRelation) { oldValue.disassociate(); return oldValue; }
        // same ids: can't update a resource. This is a sideload!
        if (eqIds) {
          const type1 = oldValue.type;
          const type2 = newValue.type;
          if (!type2 || type1 === type2) { return oldValue; }
        }
        // no old or different IDs: create new relationship
        return ResourceBase.getAsRelationship<any>(propClass, newValue, {method: 'update'});
      };

      const updateSidepost = (propClass: any, oldValue: any, newValue: any) => {
        const oldHaveId = !!(oldValue && oldValue.id);
        const newHaveId = !!(newValue && newValue.id);
        const newHaveVal = !!(newValue && Object.keys(newValue).filter(key => key !== 'id' && key !== 'type').length > 0);
        const eqIds = oldHaveId && newHaveId ? oldValue.id === newValue.id : false;

        // equal ids: update old
        if (eqIds) {
          if (newValue.deleteRelation) { oldValue.disassociate(); }
          if (newValue.destroyRelation) { oldValue.destroy(); }
          oldValue.assign(newValue);
          return oldValue;
        }
        // new have an id: change old by new
        if (newHaveId) { return ResourceBase.getAsRelationship<any>(propClass, newValue, {method: 'update'}); }
        // new don't have id but have value: create new
        if (newHaveVal) { return ResourceBase.getAsRelationship<any>(propClass, newValue, {method: 'create'}); }
        // new empty and old null: return null;
        if (!oldValue) { return null; }
        // new empty: disassociate old from the resource
        oldValue.disassociate();
        return oldValue;
      };

      const getter = function() {
        if (!this[`_${propertyName}`] && !this.modelSerialization) {
          const propClass = getClass();
          this[`_${propertyName}`] = ResourceBase.getAsRelationship<any>(propClass, {} as any);
        }
        return this[`_${propertyName}`];
      };

      const setter = function(newVal: any) {
        const propClass = getClass(newVal);

        if (this.isModelInitialization()) {
          this[`_${propertyName}`] = ResourceBase.getAsRelationship<any>(propClass, newVal, {modelInit: true});
        } else {
          const oldVal = this[`_${propertyName}`];
          this[`_${propertyName}`] = options.sidepostable ?
                                     updateSidepost(propClass, oldVal, newVal) :
                                     updateSideload(propClass, oldVal, newVal);
        }
      };

      if (delete target[propertyName]) {
        setupMetadata();
        Object.defineProperty(target, propertyName, {
          get: getter,
          set: setter,
          enumerable: true,
          configurable: true
        });
      }
    };
  }
