import { ResourceBase, Model, Attribute, HasMany } from '../../public-api';
import { ResourceMetadataWrapper } from '../resource-metadata.wrapper';


@Model({ type: 'books' })
export class Book extends ResourceBase {
  @Attribute()
  title: string;
}

@Model({ type: 'hobbies' })
export class Hobby extends ResourceBase {
  @Attribute()
  activity: string;
}

@Model({ type: 'authors' })
export class Author extends ResourceBase {
  @Attribute()
  name: string;

  @HasMany({ class: 'books', sidepostable: true })
  books: (Book | Partial<Book>)[];

  @HasMany({ class: 'hobbies' })
  hobbies: (Hobby | Partial<Hobby>)[];

  @HasMany({ class: 'books', readonly: true })
  bestSellers: (Book | Partial<Book>)[];
}


describe('HasMany decorator', () => {
  const book1Hash = { id: '11', title: 'The Shadow' };
  const book2Hash = { id: '12', title: 'Dagon' };
  const booksHash = [ book1Hash, book2Hash ];
  const hobby1Hash = { id: '21', activity: 'Fishing' };
  const hobby2Hash = { id: '22', activity: 'Golf' };
  const hobbiesHash = [ hobby1Hash, hobby2Hash ];
  const authorHash = { id: '1', name: 'Lovecraft', books: booksHash, hobbies: hobbiesHash };
  let author: Author;

  beforeEach(() => {
    author = new Author(authorHash);
  });

  it('should be instantiated with relations', () => {
    expect(author).toBeDefined();
    expect(author.id).toEqual(authorHash.id);
    expect(author.books.length).toEqual(2);
    expect(author.hobbies.length).toEqual(2);
  });

  it('should initialize as untouched', () => {
    expect(author.books.map(b => b.__method)).toEqual(['untouched', 'untouched']);
    expect(author.hobbies.map(h => h.__method)).toEqual(['untouched', 'untouched']);
  });

  it('should set be writable unless readonly is true', () => {
    const mdw = new ResourceMetadataWrapper(author);
    expect(mdw.isWritable('books')).toBeTruthy();
    expect(mdw.isWritable('bestSellers')).toBeFalsy();
  });

  describe('Sideload', () => {
    it('should be set to update if hasMany changed', () => {
      author.hobbies = [{ id: '23', activity: 'Shopping' }];
      expect(author.hobbies.length).toEqual(3);
      expect(author.hobbies[2].__method).toEqual('update');
    });

    it('should be untouched if id is same', () => {
      author.hobbies = [{ ...hobby1Hash, activity: 'new activity' }];
      expect(author.hobbies.length).toEqual(2);
      expect(author.hobbies[0].__method).toEqual('untouched');
    });

    it('should be set to disassociate', () => {
      const hobby1 = author.hobbies[0];
      const hobby2 = author.hobbies[1];
      author.hobbies = [{ id: hobby1.id, deleteRelation: true }];
      expect(author.hobbies.length).toEqual(2);
      expect(hobby1.__method).toEqual('disassociate');
      expect(hobby2.__method).toEqual('untouched');
    });

    it('should not be set to destroy', () => {
      const hobby1 = author.hobbies[0];
      const hobby2 = author.hobbies[1];
      author.hobbies = [{ id: hobby1.id, destroyRelation: true }];
      expect(author.hobbies.length).toEqual(2);
      expect(hobby1.__method).toEqual('untouched');
      expect(hobby2.__method).toEqual('untouched');
    });

    it('should stay as is when new value is null', () => {
      author.hobbies = null;
      expect(author.hobbies.length).toEqual(2);
      expect(author.hobbies.map(h => h.__method)).toEqual(['untouched', 'untouched']);
    });

    it('should stay as is when new value does not have id', () => {
      author.hobbies = [{ activity: 'new activity' }];
      expect(author.hobbies.length).toEqual(2);
      expect(author.hobbies.map(h => h.__method)).toEqual(['untouched', 'untouched']);
    });

    it('should add relation if hasMany was empty', () => {
      const hash = { ...authorHash };
      delete hash.hobbies;
      const author2 = new Author(hash);
      author2.modelSerialization = true;

      expect(author2.hobbies).toBeFalsy();
      author2.hobbies = [hobby1Hash, hobby2Hash];
      expect(author2.hobbies.length).toEqual(2);
      expect(author2.hobbies.map(h => h.__method)).toEqual(['update', 'update']);
    });

    it('should get empty array if it was null', () => {
      const hash = { ...authorHash };
      delete hash.hobbies;
      const author2 = new Author(hash);

      expect(author2.hobbies).toBeTruthy();
      expect(author2.hobbies.length).toEqual(0);
    });
  });

  describe('Sidepost', () => {
    it('should create new', () => {
      author.books = [{ title: 'The Call' }];
      expect(author.books[2].__method).toEqual('create');
    });

    it('should associate to existing ', () => {
      author.books = [{ id: '23', title: 'The Call' }];
      expect(author.books.length).toEqual(3);
      expect(author.books[2].__method).toEqual('update');
    });

    it('should be set to update if hasMany attribute changed', () => {
      const book1 = author.books[0];
      const book2 = author.books[1];
      author.books = [{ id: book1.id, title: 'New Title' }];
      expect(book2.__method).toEqual('untouched');
      expect(book1.__method).toEqual('update');
      expect(book1.title).toEqual('New Title');
    });

    it('should be untouched if new equals old', () => {
      author.books = [ book1Hash ];
      expect(author.books.map(b => b.__method)).toEqual(['untouched', 'untouched']);
    });

    it('should be set to disassociate', () => {
      const book1 = author.books[0];
      const book2 = author.books[1];
      author.books = [{ id: book1.id, deleteRelation: true }];
      expect(author.books.length).toEqual(2);
      expect(book1.__method).toEqual('disassociate');
      expect(book2.__method).toEqual('untouched');
    });

    it('should be set to destroy', () => {
      const book1 = author.books[0];
      const book2 = author.books[1];
      author.books = [{ id: book1.id, destroyRelation: true }];
      expect(author.books.length).toEqual(2);
      expect(book1.__method).toEqual('destroy');
      expect(book2.__method).toEqual('untouched');
    });

    it('should prioritize destroy', () => {
      const book1 = author.books[0];
      const book2 = author.books[1];
      author.books = [{ id: book1.id, deleteRelation: true, destroyRelation: true }];
      expect(author.books.length).toEqual(2);
      expect(book1.__method).toEqual('destroy');
      expect(book2.__method).toEqual('untouched');
    });

    it('should stay as is when new value is null', () => {
      author.books = null;
      expect(author.books.length).toEqual(2);
      expect(author.books.map(h => h.__method)).toEqual(['untouched', 'untouched']);
    });

    it('should stay as is when new value does not have id and value', () => {
      author.books = [{}];
      expect(author.books.length).toEqual(2);
      expect(author.books.map(h => h.__method)).toEqual(['untouched', 'untouched']);
    });

    it('should add relation if hasMany was empty', () => {
      const hash = { ...authorHash };
      delete hash.books;
      const author2 = new Author(hash);
      author2.modelSerialization = true;

      expect(author2.books).toBeFalsy();
      author2.books = [book1Hash, book2Hash];
      expect(author2.books.length).toEqual(2);
      expect(author2.books.map(h => h.__method)).toEqual(['update', 'update']);
    });

    it('should add relation if hasMany was empty', () => {
      const hash = { ...authorHash };
      delete hash.books;
      const author2 = new Author(hash);

      expect(author2.books).toBeTruthy();
      expect(author2.books.length).toEqual(0);
    });
  });
});
