import { PropertyConverter } from '../interfaces/property-converter.interface';

export interface ServiceDecoratorOptions {
  model: any;
  apiVersion?: string;
  endpointUrl?: string;
}

export interface ModelDecoratorOptions {
  type: string;
}

interface SharedDecoratorOptions {
  backendName?: string;
  readonly?: boolean;
}

export interface AttributeDecoratorOptions extends SharedDecoratorOptions {
  converter?: PropertyConverter;
}

export interface RelationshipDecoratorOptions extends SharedDecoratorOptions {
  class?: any;
  sidepostable?: boolean;
}
