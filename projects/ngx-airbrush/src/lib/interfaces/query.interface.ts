export interface PageScope { number?: number; size?: number; }

export type SortDir = 'asc' | 'desc';
export type SortScope = Record<string, SortDir>;

export type FieldScope = Record<string, string | string[]>;

export type FilterScope = any;

export type StatsScope = Record<string, string | string[]>;

export type IncludeArg = string | Record<string, any>;
export type IncludeScope = IncludeArg | IncludeArg[];

