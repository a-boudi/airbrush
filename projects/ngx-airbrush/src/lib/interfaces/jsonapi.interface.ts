
export type JsonapiResponse = JsonapiCollection | JsonapiDocument | JsonapiErrors;
export type JsonapiSuccess = JsonapiCollection | JsonapiDocument;
export type JsonapiRequest = JsonapiResourceRequest;

export interface JsonapiBase {
  included?: JsonapiResource[];
  meta?: Record<string, any>;
}

export interface JsonapiCollection extends JsonapiBase {
  data: JsonapiResource[];
  errors?: undefined;
}

export interface JsonapiDocument extends JsonapiBase {
  data: JsonapiResource;
  errors?: undefined;
}

export interface JsonapiResourceRequest extends JsonapiBase {
  data: JsonapiResource;
}

export interface JsonapiErrors extends JsonapiBase {
  data: undefined;
  errors: JsonapiError[];
}

export interface JsonapiResourceIdentifier {
  type: string;
  id?: string;
  temp_id?: string;
  'temp-id'?: string;
  method?: JsonapiResourceMethod;
}

export type JsonapiResourceMethod =
  | 'create'
  | 'update'
  | 'destroy'
  | 'disassociate';

export interface JsonapiResource extends JsonapiResourceIdentifier {
  attributes?: Record<string, any>;
  relationships?: Record<string, any>;
  meta?: Record<string, any>;
  links?: Record<string, any>;
}

export interface JsonapiError {
  id?: string;
  status?: string;
  code?: string;
  title?: string;
  detail?: string;
  source?: JsonapiErrorSource;
  meta: JsonapiErrorMeta;
}

export interface JsonapiErrorSource {
  pointer?: string;
  parameter?: string;
}

export type JsonapiErrorMeta = Record<string, any>;
