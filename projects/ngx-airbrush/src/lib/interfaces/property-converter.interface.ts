export interface PropertyConverter {
  serialize(value: any): any;
  deserialize(value: any): any;
  areEquals?(oldValue: any, newValue: any): boolean;
}
