import { TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Author, AuthorService } from '../../test/models/models.index';
import * as authFix from '../../test/fixtures/authors.fixture';


describe('Jsonapi Service', () => {
  let service: AuthorService;
  let httpMock: HttpTestingController;

  const auth1H = authFix.author1js;
  const auth1 = authFix.author1;
  const auth2H = authFix.author2js;
  const auth2 = authFix.author2;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ AuthorService ]
    }).compileComponents();
  });

  beforeEach(() => {
    service = TestBed.get(AuthorService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('get all', () => {
    const authsHash = { data: [
      auth1H.data,
      auth2H.data,
    ]};

    it('should return an Observable<Author[]>', async(() => {
      service.all().subscribe(authors => {
        expect(authors.length).toBe(2);

        const author1 = authors.find(auth => auth.id === auth1.id);
        const author2 = authors.find(auth => auth.id === auth2.id);
        Object.keys(auth1).forEach(key => expect(author1[key]).toEqual(auth1[key]));
        Object.keys(auth2).forEach(key => expect(author2[key]).toEqual(auth2[key]));
      });

      const req = httpMock.expectOne('localhost:4200/api/authors');
      expect(req.request.method).toBe('GET');
      req.flush(authsHash);
    }));

    it('should return with metadata Observable< { data: Author[], meta: any }>', async(() => {
      service.all({ metadata: true }).subscribe(resp => {
        const authors = resp.data;
        expect(authors.length).toBe(2);
        expect(resp.meta.stats.total.count).toBe(2);
      });

      const req = httpMock.expectOne('localhost:4200/api/authors');
      expect(req.request.method).toBe('GET');
      req.flush({ ...authsHash, meta: { stats: { total: { count: 2 }}}});
    }));

    it('should throw 4xx status response as errors', async(() => {
      service.all().subscribe(() => {}, err => {
        expect(err.status).toBeGreaterThanOrEqual(400);
      });

      const req = httpMock.expectOne('localhost:4200/api/authors');
      expect(req.request.method).toBe('GET');
      req.flush(null, { status: 400, statusText: 'Request error' });
    }));

    it('should handle 5xx errors', async(() => {
      service.all().subscribe(() => {}, err => {
        expect(err.status).toEqual(500);
        expect(err.statusText).toEqual('Internal error');
      });

      const req = httpMock.expectOne('localhost:4200/api/authors');
      req.error(new ErrorEvent('Could not retrieve authors!'), { status: 500, statusText: 'Internal error' });
    }));
  });

  describe('get one', () => {
    it('should return an Observable<Author>', async(() => {
      service.find('1').subscribe(author => {
        expect(author).toBeDefined();
        Object.keys(auth1).forEach(key => expect(author[key]).toEqual(auth1[key]));
      });

      const req = httpMock.expectOne('localhost:4200/api/authors/1');
      expect(req.request.method).toBe('GET');
      req.flush(auth1H);
    }));

    it('should throw 4xx status response as errors', async(() => {
      service.find('1').subscribe(() => {}, err => {
        expect(err.status).toBeGreaterThanOrEqual(400);
      });

      const req = httpMock.expectOne('localhost:4200/api/authors/1');
      expect(req.request.method).toBe('GET');
      req.flush(null, { status: 400, statusText: 'Request error' });
    }));

    it('should handle 5xx errors', async(() => {
      service.find('1').subscribe(() => {}, err => {
        expect(err.status).toEqual(500);
        expect(err.statusText).toEqual('Internal error');
      });

      const req = httpMock.expectOne('localhost:4200/api/authors/1');
      req.error(new ErrorEvent('Could not retrieve authors!'), { status: 500, statusText: 'Internal error' });
    }));
  });

  describe('save', () => {
    it('should post new author', async(() => {
      const model = new Author({ ...auth1, id: '' });
      service.save(model).subscribe(author => {
        expect(author).toBeDefined();
        Object.keys(auth1).forEach(key => expect(author[key]).toEqual(auth1[key]));
      });

      const req = httpMock.expectOne('localhost:4200/api/authors');
      expect(req.request.method).toBe('POST');
      req.flush(auth1H, { status: 201, statusText: 'OK' });
    }));

    it('should put existing author', async(() => {
      const model = new Author(auth1);
      service.save(model).subscribe(author => {
        expect(author).toBeDefined();
        Object.keys(auth1).forEach(key => expect(author[key]).toEqual(auth1[key]));
      });

      const req = httpMock.expectOne('localhost:4200/api/authors/1');
      expect(req.request.method).toBe('PUT');
      req.flush(auth1H, { status: 201, statusText: 'OK' });
    }));

    it('should throw 4xx status response as errors', async(() => {
      const model = new Author(auth1);
      service.save(model).subscribe(() => {}, err => {
        expect(err.status).toBeGreaterThanOrEqual(400);
      });

      const req = httpMock.expectOne('localhost:4200/api/authors/1');
      expect(req.request.method).toBe('PUT');
      req.flush(null, { status: 400, statusText: 'Request error' });
    }));

    it('should handle 5xx errors', async(() => {
      const model = new Author(auth1);
      service.save(model).subscribe(() => {}, err => {
        expect(err.status).toEqual(500);
        expect(err.statusText).toEqual('Internal error');
      });

      const req = httpMock.expectOne('localhost:4200/api/authors/1');
      req.error(new ErrorEvent('Could not retrieve authors!'), { status: 500, statusText: 'Internal error' });
    }));
  });

  describe('delete', () => {
    it('should return an Observable<Author>', async(() => {
      service.delete('1').subscribe(status => {
        expect(status.status).toEqual(204);
      });

      const req = httpMock.expectOne('localhost:4200/api/authors/1');
      expect(req.request.method).toBe('DELETE');
      req.flush(null);
    }));

    it('should throw 4xx status response as errors', async(() => {
      service.delete('1').subscribe(() => {}, err => {
        expect(err.status).toBeGreaterThanOrEqual(400);
      });

      const req = httpMock.expectOne('localhost:4200/api/authors/1');
      expect(req.request.method).toBe('DELETE');
      req.flush(null, { status: 400, statusText: 'Request error' });
    }));

    it('should handle 5xx errors', async(() => {
      service.delete('1').subscribe(() => {}, err => {
        expect(err.status).toEqual(500);
        expect(err.statusText).toEqual('Internal error');
      });

      const req = httpMock.expectOne('localhost:4200/api/authors/1');
      req.error(new ErrorEvent('Could not retrieve authors!'), { status: 500, statusText: 'Internal error' });
    }));
  });

  describe('scope', () => {
  /* tslint:disable:no-string-literal */
    it('should create new scope', () => {
      let scope = service.page(5);
      expect(scope['_pagination'].number).toEqual(5);

      scope = service.per(10);
      expect(scope['_pagination'].size).toEqual(10);

      scope = service.where({ important: true });
      expect(scope['_filter']).toEqual({ important: true });

      scope = service.stats({ total: 'count' });
      expect(scope['_stats']).toEqual({ total: 'count' });

      scope = service.select({ book: 'title' });
      expect(scope['_fields']).toEqual({ book: 'title' });

      scope = service.order('title');
      expect(scope['_sort']).toEqual({ title: 'asc' });

      scope = service.includes('books');
      expect(scope['_include']).toEqual(['books']);
    });
  /* tslint:enable:no-string-literal */
  });
});
