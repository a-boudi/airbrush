import { HttpParams } from '@angular/common/http';

import { Scope } from './scope';


class ServiceMock {
  url: string;

  public all() {}

  public find() {}

  public getModelConfig() {
    return { type: 'authors' };
  }
}


describe('Scope', () => {
  let scope: Scope<any>;
  let service: ServiceMock;
  // tslint:disable-next-line: no-string-literal
  const buildParams = (): HttpParams => scope['buildParams']();

  beforeEach(() => {
    service = new ServiceMock();
    scope = new Scope(service as any);
  });

  it('should be created', () => {
    expect(scope).toBeTruthy();
  });

  it('should create new scope', () => {
    /* tslint:disable:no-string-literal */
    let newScope = scope.page(5);
    expect(newScope).not.toBe(scope);
    expect(newScope['_pagination'].number).toEqual(5);
    expect(scope['_pagination'].number).not.toEqual(5);

    newScope = scope.per(10);
    expect(newScope).not.toBe(scope);
    expect(newScope['_pagination'].size).toEqual(10);
    expect(scope['_pagination'].size).not.toEqual(10);

    newScope = scope.where({ important: true });
    expect(newScope).not.toBe(scope);
    expect(newScope['_filter']).toEqual({ important: true });
    expect(scope['_filter']).not.toEqual({ important: true });

    newScope = scope.stats({ total: 'count' });
    expect(newScope).not.toBe(scope);
    expect(newScope['_stats']).toEqual({ total: 'count' });
    expect(scope['_stats']).not.toEqual({ total: 'count' });

    newScope = scope.select({ book: 'title' });
    expect(newScope).not.toBe(scope);
    expect(newScope['_fields']).toEqual({ book: 'title' });
    expect(scope['_fields']).not.toEqual({ book: 'title' });

    newScope = scope.order('title');
    expect(newScope).not.toBe(scope);
    expect(newScope['_sort']).toEqual({ title: 'asc' });
    expect(scope['_sort']).not.toEqual({ title: 'asc' });

    newScope = scope.includes('books');
    expect(newScope).not.toBe(scope);
    expect(newScope['_include']).toEqual(['books']);
    expect(scope['_include']).not.toEqual(['books']);
    /* tslint:enable:no-string-literal */
  });

  describe('pagination', () => {
    const key = '_pagination';

    it('should set pagination parameters', () => {
      scope = scope.page(5);
      expect(scope[key].number).toEqual(5);
      scope = scope.per(10);
      expect(scope[key].size).toEqual(10);
    });

    it('should serialize pagination parameters', () => {
      scope = scope.page(5).per(10);
      const params = buildParams();
      expect(params.has('page[number]')).toBeTruthy();
      expect(params.get('page[number]') as any).toEqual(5);

      expect(params.has('page[size]')).toBeTruthy();
      expect(params.get('page[size]') as any).toEqual(10);
    });

    it('should override parameters', () => {
      scope = scope.page(5).per(10).page(4).page(3).per(11);
      const params = buildParams();
      expect(params.has('page[number]')).toBeTruthy();
      expect(params.get('page[number]') as any).toEqual(3);

      expect(params.has('page[size]')).toBeTruthy();
      expect(params.get('page[size]') as any).toEqual(11);
    });
  });

  describe('filtering', () => {
    const key = '_filter';

    it('should set parameters', () => {
      scope = scope.where({ important: true });
      expect(scope[key]).toEqual({ important: true });
    });

    it('should set multiple parameters', () => {
      scope = scope.where({ important: true }).where({ ranking: 10 });
      expect(scope[key]).toEqual({ important: true, ranking: 10 });
    });

    it('should serialize parameters', () => {
      scope = scope.where({ important: true }).where({ ranking: 10 });
      const params = buildParams();

      expect(params.has('filter[important]')).toBeTruthy();
      expect(params.get('filter[important]') as any).toEqual(true);

      expect(params.has('filter[ranking]')).toBeTruthy();
      expect(params.get('filter[ranking]') as any).toEqual(10);
    });

    it('should override parameters', () => {
      scope = scope.where({ ranking: 10 }).where({ important: true }).where({ ranking: 20 });
      const params = buildParams();

      expect(params.has('filter[important]')).toBeTruthy();
      expect(params.get('filter[important]') as any).toEqual(true);

      expect(params.has('filter[ranking]')).toBeTruthy();
      expect(params.get('filter[ranking]') as any).toEqual(20);
    });

    it('should turn array into comma separated string', () => {
      scope = scope.where({ ranking: [10, 12, 15, 19] });
      const params = buildParams();

      expect(params.has('filter[ranking]')).toBeTruthy();
      expect(params.get('filter[ranking]')).toEqual('10,12,15,19');
    });

    it('should support nested filters', () => {
      scope = scope.where({ ranking: { gte: [10, 12] } });
      const params = buildParams();

      expect(params.has('filter[ranking]')).toBeFalsy();
      expect(params.has('filter[ranking][gte]')).toBeTruthy();
      expect(params.get('filter[ranking][gte]')).toEqual('10,12');
    });
  });

  describe('statistics', () => {
    const key = '_stats';

    it('should set parameters', () => {
      scope = scope.stats({ total: 'count' });
      expect(scope[key]).toEqual({ total: 'count' });
    });

    it('should set multiple parameters', () => {
      scope = scope.stats({ total: 'count' }).stats({ rating: '45' });
      expect(scope[key]).toEqual({ total: 'count', rating: '45' });
    });

    it('should serialize parameters', () => {
      scope = scope.stats({ total: 'count' }).stats({ rating: '45' });
      const params = buildParams();

      expect(params.has('stats[total]')).toBeTruthy();
      expect(params.get('stats[total]')).toEqual('count');

      expect(params.has('stats[rating]')).toBeTruthy();
      expect(params.get('stats[rating]')).toEqual('45');
    });
  });

  describe('selection', () => {
    const key = '_fields';

    it('should set parameters', () => {
      scope = scope.select({ book: 'title' });
      expect(scope[key]).toEqual({ book: 'title' });
    });

    it('should set multiple parameters', () => {
      scope = scope.select({ book: 'title' }).select({ author: 'lastname' });
      expect(scope[key]).toEqual({ book: 'title', author: 'lastname' });
    });

    it('should set multiple parameters with same select', () => {
      scope = scope.select({ book: 'title', author: 'lastname' });
      expect(scope[key]).toEqual({ book: 'title', author: 'lastname' });
    });

    it('should serialize parameters', () => {
      scope = scope.select({ book: 'title' }).select({ author: 'lastname' });
      const params = buildParams();

      expect(params.has('fields[book]')).toBeTruthy();
      expect(params.get('fields[book]')).toEqual('title');

      expect(params.has('fields[author]')).toBeTruthy();
      expect(params.get('fields[author]')).toEqual('lastname');
    });

    it('should override parameters', () => {
      scope = scope.select({ book: 'title' }).select({ author: 'lastname' }).select({ book: 'cover' });
      const params = buildParams();

      expect(params.get('fields[book]')).toEqual('cover');
    });

    it('should turn array into comma separated string', () => {
      scope = scope.select({ book: ['title', 'cover'] });
      const params = buildParams();

      expect(params.get('fields[book]')).toEqual('title,cover');
    });

    it('should use model type when selecting an array of strings', () => {
      scope = scope.select(['lastname', 'firstname']);
      const params = buildParams();

      expect(params.get('fields[authors]')).toEqual('lastname,firstname');
    });

    it('should use model type when selecting a string', () => {
      scope = scope.select('lastname');
      const params = buildParams();

      expect(params.get('fields[authors]')).toEqual('lastname');
    });
  });

  describe('sorting', () => {
    const key = '_sort';

    it('should set parameters', () => {
      scope = scope.order('title');
      expect(scope[key]).toEqual({ title: 'asc' });

      scope = scope.order({ title: 'asc' });
      expect(scope[key]).toEqual({ title: 'asc' });

      scope = scope.order({ title: 'desc' });
      expect(scope[key]).toEqual({ title: 'desc' });
    });

    it('should set multiple parameters', () => {
      scope = scope.order('title').order({ ranking: 'desc' });
      expect(scope[key]).toEqual({ title: 'asc', ranking: 'desc' });
    });

    it('should serialize parameters', () => {
      scope = scope.order('title').order({ ranking: 'desc' });
      const params = buildParams();

      expect(params.has('sort')).toBeTruthy();
      expect(params.get('sort')).toEqual('title,-ranking');
    });

    it('should override parameters', () => {
      scope = scope.order('title').order({ ranking: 'desc' }).order({ title: 'desc' });
      const params = buildParams();

      expect(params.has('sort')).toBeTruthy();
      expect(params.get('sort')).toEqual('-title,-ranking');
    });
  });

  describe('includes', () => {
    const key = '_include';

    it('should set parameters', () => {
      scope = scope.includes('books');
      expect(scope[key]).toEqual(['books']);

      scope = scope.includes({ books: 'chapters' });
      expect(scope[key]).toEqual(['books', { books: 'chapters' }]);
    });

    it('should set multiple parameters', () => {
      scope = scope.includes('books').includes({ books: 'chapters' });
      expect(scope[key]).toEqual(['books', { books: 'chapters' }]);
    });

    it('should serialize parameters', () => {
      scope = scope.includes('books').includes({ books: 'chapters' });
      const params = buildParams();

      expect(params.has('include')).toBeTruthy();
      expect(params.get('include')).toEqual('books,books.chapters');
    });

    it('should accept array', () => {
      scope = scope.includes(['books', { books: 'chapters' }]);
      const params = buildParams();

      expect(params.has('include')).toBeTruthy();
      expect(params.get('include')).toEqual('books,books.chapters');
    });

    it('should accept deep nested includes', () => {
      scope = scope.includes(['books', { books: 'chapters' }, { books: [{ chapters: 'paragraphs' }, 'cover'] }]);
      const params = buildParams();

      expect(params.has('include')).toBeTruthy();
      expect(params.get('include')).toEqual('books,books.chapters,books.chapters.paragraphs,books.cover');
    });

    xit('should add parent keys during nested includes', () => {
      scope = scope.includes([{ books: { chapters: 'paragraphs' }}]);
      const params = buildParams();

      expect(params.has('include')).toBeTruthy();
      expect(params.get('include')).toEqual('books,books.chapters,books.chapters.paragraphs');
    });
  });

  describe('all scopes', () => {
    beforeEach(() => {
      scope = scope.page(5).per(10);
      scope = scope.where({ important: true });
      scope = scope.stats({ total: 'count' });
      scope = scope.select({ book: 'title' });
      scope = scope.order('title');
      scope = scope.includes('books');
    });

    it('should set parameters', () => {
      /* tslint:disable:no-string-literal */
      expect(scope['_pagination'].number).toEqual(5);
      expect(scope['_pagination'].size).toEqual(10);
      expect(scope['_filter']).toEqual({ important: true });
      expect(scope['_stats']).toEqual({ total: 'count' });
      expect(scope['_fields']).toEqual({ book: 'title' });
      expect(scope['_sort']).toEqual({ title: 'asc' });
      expect(scope['_include']).toEqual(['books']);
      /* tslint:enable:no-string-literal */
    });

    it('should serialize parameters', () => {
      const params = buildParams();

      expect(params.get('page[number]') as any).toEqual(5);
      expect(params.get('page[size]') as any).toEqual(10);
      expect(params.get('filter[important]') as any).toEqual(true);
      expect(params.get('stats[total]')).toEqual('count');
      expect(params.get('fields[book]')).toEqual('title');
      expect(params.get('sort')).toEqual('title');
      expect(params.get('include')).toEqual('books');
    });

    it('should call all and find', () => {
      spyOn(service, 'all').and.callThrough();
      spyOn(service, 'find').and.callThrough();

      scope.all();
      expect(service.all).toHaveBeenCalled();

      scope.find('id');
      expect(service.find).toHaveBeenCalled();
    });
  });
});
