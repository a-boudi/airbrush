import Jsona from 'jsona';
import { v4 as uuid } from 'uuid';

import { PropertyConverter } from '../interfaces/property-converter.interface';
import { JsonapiDocument, JsonapiCollection, JsonapiResourceRequest, JsonapiResource } from '../interfaces/jsonapi.interface';
import { TJsonaModel } from 'jsona/lib/JsonaTypes';


type JsonapiBody = JsonapiDocument | JsonapiCollection | TJsonaModel | Array<TJsonaModel>;

export class JsonapiConverter implements PropertyConverter {
  private jsona = new Jsona();

  deserialize(data: JsonapiBody | any) {
    return this.jsona.deserialize(data);
  }

  serialize(data: TJsonaModel | any, includedKeys?: string[]) {
    if (!data) { return; }

    // data preparation
    this.defineTempIds(data);

    const body = this.jsona.serialize({ stuff: data, includeNames: includedKeys}) as any;
    if (!body) { return; }

    // Fix relationships and includes
    if (!body.data.id) { delete body.data.id; }
    this.addMethodsInRelations(data, body);
    this.deleteMethodsInIncludes(body);
    this.renameTempIdKeys(body);

    return body as JsonapiResourceRequest;
  }

  private defineTempIds(data: JsonapiBody) {
    this.getRelationsKeys(data).forEach(key => {
      const resources = data[key];
      this.execOnElementOrArray(resources, elt => {
        if (elt.__method === 'create') {
          elt.id = elt.id || uuid();
        }
        this.defineTempIds(elt);
      });
    });
  }

  private addMethodsInRelations(data: JsonapiBody, body: JsonapiResourceRequest) {
    this.updateMethods(data, body.data);
    this.getRelationsKeys(data).filter(key => data[key]).forEach((key: string) => {
      this.execOnElementOrArray(data[key], elt => this.updateMethodsInculdes(elt, body.included));
    });
  }

  private updateMethodsInculdes(data: TJsonaModel, includes: JsonapiResource[]) {
    const found = includes && includes.find(incl => (incl.id === data.id && incl.type === data.type));
    if (!found) { return; }

    this.updateMethods(data, found);
    this.getRelationsKeys(data).filter(key => data[key]).forEach((key: string) => {
      this.updateMethodsInculdes(data[key], includes);
    });
  }

  private updateMethods(data: TJsonaModel, resource: JsonapiResource) {
    const relations = resource.relationships;
    if (!relations) { return; }

    this.getRelationsKeys(data).filter(key => relations[key]).forEach((key: string) => {
      this.execOnElementOrArray(
        relations[key].data,
        elt => elt.method = data[key].__method,
        elt => {
          const method = data[key].find(itm => itm.id === elt.id).__method;
          elt.method = method;
        }
      );
    });
  }

  private deleteMethodsInIncludes(body: JsonapiResourceRequest) {
    const includes = body.included || [];
    includes.forEach(incl => {
      delete incl.attributes.__method;
    });
  }

  private renameTempIdKeys(body: JsonapiResourceRequest) {
    const includes = body.included;
    if (!includes) { return; }
    this.setTempId(body.data, includes);
    includes.forEach(elt => {
      this.setTempId(elt, includes);
    });
  }

  private setTempId(resource: JsonapiResource, includes: JsonapiResource[]) {
    const relations = resource.relationships;
    if (!relations) { return; }

    const keys = Object.keys(relations).filter(key => (relations.hasOwnProperty(key) && relations[key]));
    keys.forEach((key: string) => {
      this.execOnElementOrArray(
        relations[key].data, elt => {
          if (elt.method === 'create') {
            const included = includes.find(itm => itm.id === elt.id && itm.type === elt.type);
            included['temp-id'] = included.id;
            elt['temp-id'] = elt.id;
            delete included.id;
            delete elt.id;
          }
        });
    });
  }

  private getRelationsKeys(resource): string[] {
    return resource.relationshipNames || [];
  }

  private execOnElementOrArray(elements: any, callback: (elt: any) => void, callBackArray?: (elt: any) => void) {
    callBackArray = callBackArray || callback;
    if (Array.isArray(elements)) {
      elements.forEach(elt => callBackArray(elt));
    } else {
      callback(elements);
    }
  }
}
