import { JsonapiConverter } from './jsonapi.converter';

import * as authFix from '../../../test/fixtures/authors.fixture';
import * as chapFix from '../../../test/fixtures/chapters.fixture';
import * as bookFix from '../../../test/fixtures/books.fixture';


describe('Json API Converter', () => {
  let converter: JsonapiConverter;
  let bookjs;
  let book;

  beforeEach(() => {
    converter = new JsonapiConverter();
    bookjs = bookFix.book1js;
    bookjs.data.type = 'books';
    book = { id: bookjs.data.id, type: 'books', ...bookjs.data.attributes };
  });

  describe('Simple jsona functioning', () => {
    // sanity check tests
    it('should deserialize', () => {
      const body = converter.deserialize(bookjs);
      expect(body).toBeDefined();
      expect(body).toEqual(book);
    });

    it('should serialize', () => {
      const body = converter.serialize(book);
      expect(body).toBeDefined();
      expect(body).toEqual(bookjs);
    });
  });

  describe('with a single resource relationship', () => {

    describe('sideload', () => {
      let authorjs;
      let author;
      const relationsKeys = ['author'];

      beforeEach(() => {
        authorjs = authFix.author1js;
        author = { id: authorjs.data.id, type: 'authors', ...authorjs.data.attributes };
        book.author = author;
        book.relationshipNames = relationsKeys;
      });

      it('should copy method of relationship', () => {
        author.__method = 'update';
        const body = converter.serialize(book);
        expect(body.data.relationships).toBeDefined();

        const relation = body.data.relationships[relationsKeys[0]].data;
        expect(relation.id).toEqual(author.id);
        expect(relation.type).toEqual(author.type);
        expect(relation.method).toEqual(author.__method);
        expect(Object.keys(relation).length).toEqual(3);
      });
    });

    describe('sidepost', () => {
      let chapterjs;
      let chapter;
      const relationsKeys = ['firstChapter'];
      const includedKeys = ['firstChapter'];

      beforeEach(() => {
        chapterjs = chapFix.chapter1js;
        chapter = { id: chapterjs.data.id, type: 'chapters', ...chapterjs.data.attributes };
        book.firstChapter = chapter;
        book.relationshipNames = relationsKeys;
      });

      it('should copy method of relationship', () => {
        chapter.__method = 'update';
        const body = converter.serialize(book, includedKeys);
        expect(body.data.relationships).toBeDefined();

        const relation = body.data.relationships[relationsKeys[0]].data;
        expect(relation.id).toEqual(chapter.id);
        expect(relation.type).toEqual(chapter.type);
        expect(relation.method).toEqual(chapter.__method);
        expect(Object.keys(relation).length).toEqual(3);
      });

      it('should add included', () => {
        chapter.__method = 'update';
        const body = converter.serialize(book, includedKeys);
        expect(body.included).toBeDefined();

        const included = body.included;
        expect(included.length).toEqual(1);
        expect(included[0].id).toEqual(chapter.id);
        expect(included[0].attributes).toEqual(chapterjs.data.attributes);
      });

      it('should set temp-id when creating', () => {
        chapter.id = '';
        chapter.__method = 'create';
        const body = converter.serialize(book, includedKeys);
        expect(body.data.relationships).toBeDefined();

        const relation = body.data.relationships[relationsKeys[0]].data;
        expect(relation.id).toBeUndefined();
        expect(relation['temp-id']).toBeDefined();
        expect(relation.type).toEqual(chapter.type);
        expect(relation.method).toEqual(chapter.__method);
        expect(Object.keys(relation).length).toEqual(3);

        const included = body.included;
        expect(included.length).toEqual(1);
        expect(included[0].id).toBeUndefined();
        expect(included[0]['temp-id']).toBeDefined();
        expect(included[0].attributes).toEqual(chapterjs.data.attributes);

        expect(relation['temp-id']).toEqual(included[0]['temp-id']);
      });

      it('should copy method of nested relationship', () => {
        chapter.__method = 'update';
        chapter.relationshipNames = ['firstParagraph'];
        const paragraph = { id: '41', type: 'paragraphs', __method: 'update' };
        chapter.firstParagraph = paragraph;

        const body = converter.serialize(book, [...includedKeys, 'firstChapter.firstParagraph']);
        const included = body.included.find(elt => elt.type === 'chapters');
        expect(included).toBeDefined();

        const relation = included.relationships.firstParagraph.data;
        expect(relation.id).toEqual(paragraph.id);
        expect(relation.type).toEqual(paragraph.type);
        expect(relation.method).toEqual(paragraph.__method);
        expect(Object.keys(relation).length).toEqual(3);
      });

      it('should set temp-id of nested creation', () => {
        chapter.__method = 'update';
        chapter.relationshipNames = ['firstParagraph'];
        const paragraph = { type: 'paragraphs', __method: 'create', text: 'lorem ipsum' };
        chapter.firstParagraph = paragraph;

        const body = converter.serialize(book, [...includedKeys, 'firstChapter.firstParagraph']);
        let included = body.included.find(elt => elt.type === chapter.type && elt.id === chapter.id);
        expect(included).toBeDefined();

        const relation = included.relationships.firstParagraph.data;
        expect(relation.id).toBeUndefined();
        expect(relation['temp-id']).toBeDefined();
        expect(relation.type).toEqual(paragraph.type);
        expect(relation.method).toEqual(paragraph.__method);
        expect(Object.keys(relation).length).toEqual(3);

        included = body.included.find(elt => elt.type === paragraph.type);
        expect(included.id).toBeUndefined();
        expect(included['temp-id']).toBeDefined();
        expect(included['temp-id']).toEqual(relation['temp-id']);
      });
    });
  });

  describe('with a multiple resource relationship', () => {
    let chapter1js;
    let chapter1;
    let chapter2js;
    let chapter2;
    const relationsKeys = ['chapters'];

    beforeEach(() => {
      chapter1js = chapFix.chapter1js;
      chapter1 = { id: chapter1js.data.id, type: 'chapters', ...chapter1js.data.attributes };
      chapter2js = chapFix.chapter2js;
      chapter2 = { id: chapter2js.data.id, type: 'chapters', ...chapter2js.data.attributes };
      book.chapters = [chapter1, chapter2];
      book.relationshipNames = relationsKeys;
    });

    describe('sideload', () => {
      it('should copy method of relationship', () => {
        chapter1.__method = 'update';
        chapter2.__method = 'disassociate';
        const body = converter.serialize(book);
        expect(body.data.relationships).toBeDefined();

        const relations = body.data.relationships[relationsKeys[0]].data;
        expect(relations.length).toEqual(2);
        const relation1 = relations.find(rel => rel.id === chapter1.id);
        const relation2 = relations.find(rel => rel.id === chapter2.id);

        expect(relation1).toBeDefined();
        expect(relation1.type).toEqual(chapter1.type);
        expect(relation1.method).toEqual(chapter1.__method);
        expect(Object.keys(relation1).length).toEqual(3);

        expect(relation2).toBeDefined();
        expect(relation2.type).toEqual(chapter2.type);
        expect(relation2.method).toEqual(chapter2.__method);
        expect(Object.keys(relation2).length).toEqual(3);

      });
    });

    describe('sidepost', () => {
      const includedKeys = ['chapters'];

      it('should copy method of relationship', () => {
        chapter1.__method = 'update';
        chapter2.__method = 'disassociate';
        const body = converter.serialize(book, includedKeys);
        expect(body.data.relationships).toBeDefined();

        const relations = body.data.relationships[relationsKeys[0]].data;
        expect(relations.length).toEqual(2);
        const relation1 = relations.find(rel => rel.id === chapter1.id);
        const relation2 = relations.find(rel => rel.id === chapter2.id);

        expect(relation1).toBeDefined();
        expect(relation1.type).toEqual(chapter1.type);
        expect(relation1.method).toEqual(chapter1.__method);
        expect(Object.keys(relation1).length).toEqual(3);

        expect(relation2).toBeDefined();
        expect(relation2.type).toEqual(chapter2.type);
        expect(relation2.method).toEqual(chapter2.__method);
        expect(Object.keys(relation2).length).toEqual(3);
      });

      it('should add included', () => {
        chapter1.__method = 'update';
        chapter2.__method = 'update';
        const body = converter.serialize(book, includedKeys);
        expect(body.included).toBeDefined();

        const included = body.included;
        expect(included.length).toEqual(2);
        const include1 = included.find(rel => rel.id === chapter1.id);
        const include2 = included.find(rel => rel.id === chapter2.id);
        expect(include1.attributes).toEqual(chapter1js.data.attributes);
        expect(include2.attributes).toEqual(chapter2js.data.attributes);
      });

      it('should set temp-id when creating', () => {
        chapter1.id = '';
        chapter1.__method = 'create';
        chapter2.__method = 'update';
        const body = converter.serialize(book, includedKeys);
        expect(body.included).toBeDefined();

        const relations = body.data.relationships[relationsKeys[0]].data;
        expect(relations.length).toEqual(2);
        const relation1 = relations.find(rel => !rel.id);
        const relation2 = relations.find(rel => rel.id === chapter2.id);
        expect(relation1.id).toBeUndefined();
        expect(relation1['temp-id']).toBeDefined();
        expect(relation1.method).toEqual(chapter1.__method);
        expect(relation2.id).toEqual(chapter2.id);
        expect(relation2.method).toEqual(chapter2.__method);

        const included = body.included;
        expect(included.length).toEqual(2);
        const include1 = included.find(rel => !rel.id);
        const include2 = included.find(rel => rel.id === chapter2.id);
        expect(include1.id).toBeUndefined();
        expect(include1['temp-id']).toBeDefined();
        expect(include1.attributes).toEqual(chapter1js.data.attributes);
        expect(include2.attributes).toEqual(chapter2js.data.attributes);

        expect(relation1['temp-id']).toEqual(include1['temp-id']);
      });

      it('should copy method of nested relationship', () => {
        chapter1.__method = 'update';
        chapter1.relationshipNames = ['paragraphs'];
        const paragraph = { id: '41', type: 'paragraphs', __method: 'update' };
        chapter1.paragraphs = [paragraph];

        const body = converter.serialize(book, [...includedKeys, 'chapters.paragraphs']);
        const included = body.included.find(elt => elt.type === chapter1.type && elt.id === chapter1.id);
        expect(included).toBeDefined();

        const relation = included.relationships.paragraphs.data;
        expect(Array.isArray(relation)).toBeTruthy();
        expect(relation[0].id).toEqual(paragraph.id);
        expect(relation[0].type).toEqual(paragraph.type);
        expect(relation[0].method).toEqual(paragraph.__method);
        expect(Object.keys(relation[0]).length).toEqual(3);
      });

      it('should set temp-id of nested creation', () => {
        chapter1.__method = 'update';
        chapter1.relationshipNames = ['paragraphs'];
        const paragraph1 = { id: '', type: 'paragraphs', __method: 'create', text: 'lorem ipsum' };
        const paragraph2 = { id: '', type: 'paragraphs', __method: 'create', text: 'dolor sit amet' };
        chapter1.paragraphs = [paragraph1, paragraph2];

        const body = converter.serialize(book, [...includedKeys, 'chapters.paragraphs']);
        const included = body.included.find(elt => elt.type === chapter1.type && elt.id === chapter1.id);
        expect(included).toBeDefined();

        const relation = included.relationships.paragraphs.data;
        expect(Array.isArray(relation)).toBeTruthy();
        expect(relation[0].id).toBeUndefined();
        expect(relation[0]['temp-id']).toBeDefined();
        expect(relation[0].type).toEqual(paragraph1.type);
        expect(relation[0].method).toEqual(paragraph1.__method);
        expect(Object.keys(relation[0]).length).toEqual(3);

        expect(relation[1].id).toBeUndefined();
        expect(relation[1]['temp-id']).toBeDefined();
        expect(relation[1].type).toEqual(paragraph2.type);
        expect(relation[1].method).toEqual(paragraph2.__method);
        expect(Object.keys(relation[0]).length).toEqual(3);

        expect(relation[0]['temp-id']).not.toEqual(relation[1]['temp-id']);
      });
    });
  });
});
