import { PropertyConverter } from '../interfaces/property-converter.interface';


export class DateConverter implements PropertyConverter {
  deserialize(value: string | number | Date ) {
    if (!value) { return null; }
    return new Date(value);
  }

  serialize(value: Date) {
    if (!value) { return null; }
    return value.toJSON();
  }
}
