import { JsonapiResourceRequest } from './interfaces/jsonapi.interface';
import { ResourceBase } from './resource-base.model';
import { AttributeMetadata } from './decorators/symboles';
import { ResourceMetadataWrapper } from './resource-metadata.wrapper';
import { JsonapiConverter } from './converters/jsonapi.converter';


export class ResourceSerializer {
  private mdwrapper: ResourceMetadataWrapper;

  constructor(private model: ResourceBase) {
    this.mdwrapper = new ResourceMetadataWrapper(model);
  }

  public toJsonapi(): JsonapiResourceRequest {
    const result = this.serialize();
    const includeKeys = this.mdwrapper.getIncludedKeys();

    const body = new JsonapiConverter().serialize(result, includeKeys);
    return body;
  }

  public serialize() {
    const type = this.model.type;

    const [attributes, belongstos, hasManys] = this.serializeAll();
    const relationshipNames = [];
    relationshipNames.push(...Object.keys(belongstos).filter(key => belongstos.hasOwnProperty(key)));
    relationshipNames.push(...Object.keys(hasManys).filter(key => hasManys.hasOwnProperty(key)));
    const result: { [attrName: string]: any; } = {
      id: this.model.id,
      type,
      ...attributes,
      ...belongstos,
      ...hasManys,
      relationshipNames,
    };
    if (this.model.isRelation) {
      result.__method = this.model.relationMethod;
    }
    return result;
  }

  public toHash(): { [attrName: string]: any; } {
    const res = { id: this.model.id, type: this.model.type };
    this.mdwrapper.attributesKeys({ defined: true }).forEach(key => res[key] = this.model[key]);
    this.mdwrapper.belongsToKeys({ defined: true }).forEach(key => res[key] = this.model[key].toHash());
    this.mdwrapper.hasManyKeys({ defined: true }).forEach(key => {
      this.model[key].forEach(elt => {
        res[key] = res[key] || []; // no need to init res[key] if array has no elements.
        res[key].push(elt.toHash());
      });
    });
    return res;
  }

  private serializeAll(): { [attrName: string]: any; }[] {
    const attributes = this.serializeAttributes();
    const belongstos = this.serializeBelongsTo();
    const hasManys = this.serializeHasMany();
    return [attributes, belongstos, hasManys];
  }

  private serializeAttributes(): { [attrName: string]: any; } {
    let keys = this.mdwrapper.attributesKeys({ defined: true });
    keys = keys.filter(key => this.mdwrapper.isWritable(key));

    const attributes = this.model[AttributeMetadata];
    keys = this.model.id ? keys.filter(key => attributes[key].hasDirtyAttributes) : keys;
    const result = {};
    keys.forEach(key => {
      const beKey = this.mdwrapper.backendPropName(key);
      result[beKey] = attributes[key].serializedValue;
    });
    return result;
  }

  private serializeBelongsTo(): { [attrName: string]: any; } {
    let keys = this.mdwrapper.belongsToKeys({ defined: true });
    keys = keys.filter(key => this.mdwrapper.isWritable(key));

    // Ignore untouched relationships
    keys = keys.filter(key => this.model[key].hasBeenTouched);
    const belongstos = {};
    keys.forEach(key => {
      const sidepostable = this.mdwrapper.isSidepostable(key);
      const beKey = this.mdwrapper.backendPropName(key);
      belongstos[beKey] = sidepostable ? this.model[key].serialize() : this.model[key].sideload();
      if (!belongstos[beKey]) { delete belongstos[beKey]; }
    });
    return belongstos;
  }

  private serializeHasMany(): { [attrName: string]: any; } {
    let keys = this.mdwrapper.hasManyKeys({ defined: true });
    keys = keys.filter(key => this.mdwrapper.isWritable(key));

    const hasManys = {};
    keys.forEach(key => {
      const sidepostable = this.mdwrapper.isSidepostable(key);
      const beKey = this.mdwrapper.backendPropName(key);
      hasManys[beKey] = sidepostable ?
                        this.model[key].filter(elt => elt.hasBeenTouched).map(elt => elt.serialize()) :
                        this.model[key].filter(elt => elt.hasBeenTouched).map(elt => elt.sideload());
      if (hasManys[beKey].length === 0) { delete hasManys[beKey]; }
    });
    return hasManys;
  }
}
