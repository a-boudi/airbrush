import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { JsonapiBaseService } from './jsonapi-base.service';
import { ResourceBase } from './resource-base.model';
import {
  PageScope,
  SortScope,
  FieldScope,
  FilterScope,
  StatsScope,
  IncludeScope,
  IncludeArg,
} from './interfaces/query.interface';


export class Scope<T extends ResourceBase> {
  private service: JsonapiBaseService<T>;

  /* tslint:disable:variable-name */
  private _pagination: PageScope = {};
  private _filter: FilterScope = {};
  private _sort: SortScope = {};
  private _fields: FieldScope = {};
  private _include: IncludeArg[] = [];
  private _stats: StatsScope = {};
  /* tslint:enable:variable-name */

  private static clone<T extends ResourceBase>(old: Scope<T>): Scope<T> {
    const clone = new Scope(old.service);
    const clonedAttr = cloneDeep(old, 'service');
    clone._pagination = clonedAttr._pagination;
    clone._filter     = clonedAttr._filter;
    clone._sort       = clonedAttr._sort;
    clone._fields     = clonedAttr._fields;
    clone._include    = clonedAttr._include;
    clone._stats      = clonedAttr._stats;
    return clone;
  }

  constructor(service: JsonapiBaseService<T>) {
    this.service = service;
  }

  public all(): Observable<T[]>;
  public all(options: { metadata: true }): Observable<{ data: T[], meta: any }>;

  public all(options?: { metadata?: boolean }): Observable<T[]> | Observable<{ data: T[], meta: any }> {
    const params = this.buildParams();
    options = options || {};
    return this.service.all({ params, ...options });
  }

  public find(id: string): Observable<T> {
    const params = this.buildParams();
    return this.service.find(id, params);
  }

  public page(pageNumber: number): Scope<T> {
    const clone = Scope.clone(this);

    clone._pagination.number = pageNumber;
    return clone;
  }

  public per(size: number): Scope<T> {
    const clone = Scope.clone(this);

    clone._pagination.size = size;
    return clone;
  }

  public where(clause: FilterScope): Scope<T> {
    const clone = Scope.clone(this);

    for (const key in clause) {
      if (clause.hasOwnProperty(key)) {
        clone._filter[key] = clause[key];
      }
    }
    return clone;
  }

  public stats(clause: StatsScope): Scope<T> {
    const clone = Scope.clone(this);

    for (const key in clause) {
      if (clause.hasOwnProperty(key)) {
        clone._stats[key] = clause[key];
      }
    }
    return clone;
  }

  public order(clause: SortScope | string): Scope<T> {
    const clone = Scope.clone(this);

    if (typeof clause === 'string') {
      clone._sort[clause] = 'asc';
      return clone;
    } // else
    for (const key in clause) {
      if (clause.hasOwnProperty(key)) {
        clone._sort[key] = clause[key];
      }
    }

    return clone;
  }

  public select(clause: FieldScope | string | string[]): Scope<T> {
    const clone = Scope.clone(this);

    if (Array.isArray(clause) || typeof clause === 'string') {
      const type = clone.service.getModelConfig().type;
      clone._fields[type] = clause;
      return clone;
    } // else
    for (const key in clause) {
      if (clause.hasOwnProperty(key)) {
        clone._fields[key] = clause[key];
      }
    }

    return clone;
  }

  public includes(clause: IncludeScope): Scope<T> {
    const clone = Scope.clone(this);

    if (Array.isArray(clause)) {
      clone._include.push(...clause);
    } else {
      clone._include.push(clause);
    }

    return clone;
  }

  private buildParams(): HttpParams {
    const params = {
      page: this._pagination,
      filter: this._filter,
      sort: this.stringifySorts(this._sort),
      fields: this._fields,
      stats: this._stats,
      include: this.stringifyIncludes(this._include),
    };
    return this.serializeParams(params);
  }

  private stringifySorts(sorts: SortScope | undefined): string[] {
    if (!sorts || Object.keys(sorts).length === 0) { return []; }

    const params: string[] = [];

    for (let key in sorts) {
      if (sorts.hasOwnProperty(key)) {
        if (sorts[key] !== 'asc') { key = `-${key}`; }
        params.push(key);
      }
    }

    return params;
  }

  private stringifyIncludes(includes: IncludeScope | undefined): string[] {
    if (!includes || (Object.keys(includes).length === 0 && includes.length === 0)) { return []; }
    const params: string[] = [];

    if (Array.isArray(includes)) {
      const array = includes.map(elt => this.stringifyIncludes(elt)).reduce((res, elt1) => { res.push(...elt1); return res; }, []);
      params.push(...array);
      return params;
    } // else
    if (typeof includes === 'object') {
      for (const key in includes) {
        if (includes.hasOwnProperty(key)) {
          const array = this.stringifyIncludes(includes[key]).map(incl => `${key}.${incl}`);
          params.push(...array);
        }
      }
      return params;
    } // else (is a string)
    params.push(includes);
    return params;
  }

  private serializeParams(obj: any, params?: HttpParams): HttpParams {
    if (!params) { params = new HttpParams(); }
    for (const key in obj) {
      if (!obj.hasOwnProperty(key) || !obj[key]) { continue; }

      const value = obj[key];

      if (Array.isArray(value)) {
        if (value.length > 0) {
          params = params.append(key, value.join(','));
        }
        continue;
      }
      // else
      if (typeof value === 'object') {
        for (const subKey in value) {
          if (!value.hasOwnProperty(subKey)) { continue; }

          const elt = {};
          const k = `${key}[${subKey}]`;
          elt[k] = value[subKey];
          params = this.serializeParams(elt, params);
        }
        continue;
      }
      // else
      params = params.append(key, value);
    }

    return params;
  }
}

function cloneDeep(obj, omit?: string | string[]) {
  omit = omit || [];
  omit = typeof omit === 'string' ? [omit] : omit;

  // Handle simple types
  if (!obj || typeof obj !== 'object') { return obj; }

  // Handle Date
  if (obj instanceof Date) {
    const copy = new Date();
    copy.setTime(obj.getTime());
    return copy;
  }

  // Handle Array
  if (obj instanceof Array) {
    const copy = [];
    for (let i = 0, len = obj.length; i < len; i++) {
      copy[i] = cloneDeep(obj[i]);
    }
    return copy;
  }

  // Handle Object
  if (obj instanceof Object) {
    const copy = {};
    Object.keys(obj).filter(key => !omit.includes(key)).forEach(key => {
      copy[key] = cloneDeep(obj[key]);
    });
    return copy;
  }

  throw new Error('Unable to copy obj! Its type isn\'t supported.');
}
