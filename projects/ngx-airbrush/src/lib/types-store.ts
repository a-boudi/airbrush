interface ResourcesDict { [key: string]: any; }

export class TypesStore {
  private static instance: TypesStore = null;

  // tslint:disable-next-line: variable-name
  private _types: ResourcesDict = {};

  private constructor() {
  }

  static getInstance() {
    if (!TypesStore.instance) {
      TypesStore.instance = new TypesStore();
    }

    return TypesStore.instance;
  }

  static add(key: string, obj: any) {
    TypesStore.getInstance().add(key, obj);
  }

  static find(key: string) {
    return TypesStore.getInstance().find(key);
  }

  static all(): ResourcesDict {
    return TypesStore.getInstance().all;
  }

  add(key: string, obj: any) {
    this._types[key] = obj;
  }

  find(key: string): any {
    return this.all[key];
  }

  get all(): ResourcesDict { return this._types; }
}
